package be.ac.umons.components;

import be.ac.umons.enumerations.PowerStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PowerComponentTest {

    PowerComponent powerComponent = new PowerComponent();

    @Test
    public void getInstance() {
        assertNotNull(powerComponent);
    }

    @Test
    public void changeStatus_OFF() {
        powerComponent.changeStatus();
        assertEquals(PowerStatus.ON, powerComponent.getPowerStatus());
    }

    @Test
    public void changeStatus_ON() {
        powerComponent.changeStatus();
        powerComponent.changeStatus();
        assertEquals(PowerStatus.OFF, powerComponent.getPowerStatus());
    }

    @Test
    public void getPowerStatus() {
        assertEquals(PowerStatus.OFF, powerComponent.getPowerStatus());
    }
}