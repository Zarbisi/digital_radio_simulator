package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BluetoothComponentTest {

    BluetoothComponent bluetoothComponent = new BluetoothComponent();

    @Test
    public void next() {
        bluetoothComponent.setIndexSong(0);
        bluetoothComponent.next();
        assertEquals("ACDC - ThunderStruck", bluetoothComponent.getActualSong());
    }

    @Test
    public void nextAtMax() {
        bluetoothComponent.setIndexSong(bluetoothComponent.getSongs().size() - 1);
        bluetoothComponent.next();
        assertEquals("ACDC - Back in Black", bluetoothComponent.getActualSong());
    }

    @Test
    public void previousAtMin() {
        bluetoothComponent.setIndexSong(0);
        bluetoothComponent.previous();
        assertEquals("Skip the use - Ghost", bluetoothComponent.getActualSong());
    }

    @Test
    public void previous() {
        bluetoothComponent.setIndexSong(2);
        bluetoothComponent.previous();
        assertEquals("ACDC - ThunderStruck", bluetoothComponent.getActualSong());
    }

    @Test
    public void getActualSong() {
        bluetoothComponent.setIndexSong(4);
        assertEquals("Skip the use - Get Papers", bluetoothComponent.getActualSong());
    }

}