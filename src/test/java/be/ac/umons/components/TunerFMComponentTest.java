package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TunerFMComponentTest {
    TunerFMComponent tunerFMComponent = new TunerFMComponent();

    @Test
    public void getInstance() {
        assertNotNull(tunerFMComponent);
    }

    @Test
    public void increaseFrequency() {
        tunerFMComponent.setFrequencyIndex(0);
        tunerFMComponent.increaseFrequency();
        assertEquals("87.80", tunerFMComponent.getFrequency());
    }

    @Test
    public void increaseFrequencyAtMax() {
        tunerFMComponent.setFrequencyIndex(tunerFMComponent.getStations().size() - 1);
        tunerFMComponent.increaseFrequency();
        assertEquals("87.70", tunerFMComponent.getFrequency());
    }

    @Test
    public void decreaseFrequency() {
        tunerFMComponent.setFrequencyIndex(5);
        tunerFMComponent.decreaseFrequency();
        assertEquals("88.70", tunerFMComponent.getFrequency());
    }

    @Test
    public void decreaseFrequencyAtMin() {
        tunerFMComponent.setFrequencyIndex(0);
        tunerFMComponent.decreaseFrequency();
        assertEquals("107.90", tunerFMComponent.getFrequency());
    }

    @Test
    public void setFrequencyIndex() {
        tunerFMComponent.setFrequencyIndex(3);
        assertEquals(3, tunerFMComponent.getFrequencyIndex());
    }

    @Test
    public void getActualRadio() {
        tunerFMComponent.setFrequencyIndex(0);
        assertEquals("Ramdam Musique", tunerFMComponent.getActualRadio());
    }
}