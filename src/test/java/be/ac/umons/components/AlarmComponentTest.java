package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AlarmComponentTest {

    AlarmComponent alarmComponent = new AlarmComponent();

    @Test
    public void toggleAlarmOn() {
        assertEquals(true, alarmComponent.toggleAlarm());
    }

    @Test
    public void toggleAlarmOff() {
        alarmComponent.toggleAlarm();
        assertEquals(false, alarmComponent.toggleAlarm());
    }

    @Test
    public void getHour() {
        alarmComponent.setHour(0);
        assertEquals(0, alarmComponent.getHour());
    }

    @Test
    public void setHourMinusZero() {
        alarmComponent.setHour(-1);
        assertEquals(0, alarmComponent.getHour());
    }

    @Test
    public void setHourMoreThanTwentyThree() {
        alarmComponent.setHour(120);
        assertEquals(23, alarmComponent.getHour());
    }

    @Test
    public void setHour() {
        alarmComponent.setHour(12);
        assertEquals(12, alarmComponent.getHour());
    }

    @Test
    public void getMinute() {
        alarmComponent.setMinute(0);
        assertEquals(0, alarmComponent.getMinute());
    }

    @Test
    public void setMinuteMoreThanSixty() {
        alarmComponent.setMinute(360);
        assertEquals(59, alarmComponent.getMinute());
    }

    @Test
    public void setMinuteLessThanZero() {
        alarmComponent.setMinute(-12);
        assertEquals(0, alarmComponent.getMinute());
    }

    @Test
    public void setMinute() {
        alarmComponent.setMinute(35);
        assertEquals(35, alarmComponent.getMinute());
    }
}