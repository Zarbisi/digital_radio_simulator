package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UsbComponentTest {

    UsbComponent usbComponent = new UsbComponent();

    @Test
    public void next() {
        usbComponent.setIndexSong(1);
        usbComponent.next();
        assertEquals("Aaron Smith, Krono - Dancin - Krono Remix", usbComponent.getActualSong());
    }

    @Test
    public void nextAtMax() {
        usbComponent.setIndexSong(usbComponent.getSongs().size() - 1);
        usbComponent.next();
        assertEquals("The Weeknd - Scared To Live", usbComponent.getActualSong());
    }

    @Test
    public void previous() {
        usbComponent.setIndexSong(1);
        usbComponent.previous();
        assertEquals("The Weeknd - Scared To Live", usbComponent.getActualSong());
    }

    @Test
    public void previousAtMin() {
        usbComponent.setIndexSong(0);
        usbComponent.previous();
        assertEquals("Owl City - Not All Heroes Wear Cape", usbComponent.getActualSong());
    }

    @Test
    public void setIndexSong() {
        usbComponent.setIndexSong(0);
        assertEquals(0, usbComponent.getIndexSong());
    }
}