package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AdditionalSpeakerComponentTest {

    AdditionalSpeakerComponent additionalSpeaker = new AdditionalSpeakerComponent();

    @Test
    public void increaseBalanceAtMax() {
        additionalSpeaker.setBalance(10);
        additionalSpeaker.increaseBalance();
        assertEquals(10, additionalSpeaker.getBalance());
    }
    @Test
    public void increaseBalance() {
        additionalSpeaker.setBalance(0);
        additionalSpeaker.increaseBalance();
        assertEquals(1, additionalSpeaker.getBalance());
    }

    @Test
    public void decreaseBalanceAtMax() {
        additionalSpeaker.setBalance(-10);
        additionalSpeaker.decreaseBalance();
        assertEquals(-10, additionalSpeaker.getBalance());
    }

    @Test
    public void decreaseBalance() {
        additionalSpeaker.setBalance(0);
        additionalSpeaker.decreaseBalance();
        assertEquals(-1, additionalSpeaker.getBalance());
    }
}