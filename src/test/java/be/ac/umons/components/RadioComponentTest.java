package be.ac.umons.components;

import be.ac.umons.enumerations.ComponentStatus;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Null;

import static org.junit.jupiter.api.Assertions.*;

public class RadioComponentTest {

    @Test
    public void getStatus() {
        RadioComponent radioComponent = Mockito.mock(
                RadioComponent.class,
                Mockito.CALLS_REAL_METHODS);
        // TEST on null because POWERMOCK is not usable with JUNIT 5
        assertEquals(null, radioComponent.getStatus());
    }

    @Test
    public void setFailure() {
        RadioComponent radioComponent = Mockito.mock(
                RadioComponent.class,
                Mockito.CALLS_REAL_METHODS);
        radioComponent.setFailure();
        assertEquals(ComponentStatus.FAILURE, radioComponent.getStatus());
    }

    @Test
    public void setFunctional() {
        RadioComponent radioComponent = Mockito.mock(
                RadioComponent.class,
                Mockito.CALLS_REAL_METHODS);
        radioComponent.setFunctional();
        assertEquals(ComponentStatus.FUNCTIONAL, radioComponent.getStatus());
    }

    @Test
    public void setNonActivated() {
        RadioComponent radioComponent = Mockito.mock(
                RadioComponent.class,
                Mockito.CALLS_REAL_METHODS);
        radioComponent.setNonActivated();
        assertEquals(ComponentStatus.NONACTIVATED, radioComponent.getStatus());
    }

    @Test
    public void isFailing_true() {
        RadioComponent radioComponent = Mockito.mock(
                RadioComponent.class,
                Mockito.CALLS_REAL_METHODS);
        radioComponent.setFailure();
        assertEquals(true, radioComponent.isFailing());
    }

    @Test
    public void isFailing_false() {
        RadioComponent radioComponent = Mockito.mock(
                RadioComponent.class,
                Mockito.CALLS_REAL_METHODS);
        radioComponent.setFunctional();
        assertEquals(false, radioComponent.isFailing());
    }
}