package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BreakingNewsComponentTest {

    BreakingNewsComponent breakingNewsComponent = new BreakingNewsComponent();

    @Test
    public void getBroadcastNews() {
        List<String> list = new ArrayList<>();
        list.add("Test");
        breakingNewsComponent.setNews(list);
        assertEquals("Test", breakingNewsComponent.getBroadcastNews());
    }
}