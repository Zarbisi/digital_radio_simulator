package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SpeakerComponentTest {

    SpeakerComponent speakerComponent = new SpeakerComponent();

    @Test
    public void getInstance() {
        assertNotNull(speakerComponent);
    }

    @Test
    public void increaseVolumeAtMax() {
        speakerComponent.setVolume(20);
        speakerComponent.increaseVolume();
        assertEquals(20, speakerComponent.getVolume());
    }

    @Test
    public void increaseVolume() {
        speakerComponent.setVolume(10);
        speakerComponent.increaseVolume();
        assertEquals(11, speakerComponent.getVolume());
    }

    @Test
    public void decreaseVolume() {
        speakerComponent.setVolume(10);
        speakerComponent.decreaseVolume();
        assertEquals(9, speakerComponent.getVolume());
    }

    @Test
    public void decreaseVolumeAtMin() {
        speakerComponent.setVolume(0);
        speakerComponent.decreaseVolume();
        assertEquals(0, speakerComponent.getVolume());
    }
}