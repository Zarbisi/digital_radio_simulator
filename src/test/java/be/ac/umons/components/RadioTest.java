package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RadioTest {

    Radio radio = Radio.getInstance();

    @Test
    public void getInstance() {
        assertNotNull(radio);
    }

    @Test
    public void toggleAlarm() {
        assertEquals(radio.toggleAlarm(), radio.getAlarm().isActivated());
    }

    @Test
    public void switchOnOff() {
        assertEquals(radio.switchOnOff(), radio.getPower().getPowerStatus());
    }

    @Test
    public void getPowerStatus() {
        assertEquals(radio.getPowerStatus(), radio.getPower().getPowerStatus());
    }

    @Test
    public void increaseVolume() {
        radio.getSpeaker().setVolume(10);
        assertEquals(radio.increaseVolume(), 11);
    }

    @Test
    public void decreaseVolume() {
        radio.getSpeaker().setVolume(10);
        assertEquals(radio.decreaseVolume(), 9);
    }

    @Test
    public void increaseBalance() {
        radio.getSecondSpeaker().setBalance(0);
        assertEquals(radio.increaseBalance(), 1);
    }

    @Test
    public void decreaseBalance() {
        radio.getSecondSpeaker().setBalance(0);
        assertEquals(radio.decreaseBalance(), -1);
    }

    @Test
    public void nextChannel() {
        String previousChannel = radio.getTuner().getActualChannel();
        radio.nextChannel();
        assertNotEquals(previousChannel, radio.getActualChannel());
    }

    @Test
    public void previousChannel() {
        String nextChannel = radio.getTuner().getActualChannel();
        radio.previousChannel();
        assertNotEquals(nextChannel, radio.getActualChannel());
    }

    @Test
    public void increaseFrequency() {
        String nextFrequency = radio.getTunerFM().getFrequency();
        radio.increaseFrequency();
        assertNotEquals(nextFrequency, radio.getActualFrequency());
    }

    @Test
    public void decreaseFrequency() {
        String previousFrequency = radio.getTunerFM().getFrequency();
        radio.decreaseFrequency();
        assertNotEquals(previousFrequency, radio.getActualFrequency());
    }

    @Test
    public void nextSong_bluetooth() {
        radio.nextSong(2);
        assertEquals(radio.getBluetooth().getActualSong(), radio.getSong(2));
    }

    @Test
    public void previousSong_bluetooth() {
        radio.previousSong(2);
        assertEquals(radio.getBluetooth().getActualSong(), radio.getSong(2));
    }

    @Test
    public void nextSong_aux() {
        radio.nextSong(3);
        assertEquals(radio.getAux().getActualSong(), radio.getSong(3));
    }

    @Test
    public void previousSong_aux() {
        radio.previousSong(3);
        assertEquals(radio.getAux().getActualSong(), radio.getSong(3));
    }

    @Test
    public void nextSong_usb() {
        radio.nextSong(4);
        assertEquals(radio.getUsb().getActualSong(), radio.getSong(4));
    }

    @Test
    public void previousSong_usb() {
        radio.previousSong(4);
        assertEquals(radio.getUsb().getActualSong(), radio.getSong(4));
    }

    @Test
    public void nextSong_null() {
        radio.nextSong(99);
        assertEquals(null, radio.getSong(99));
    }

    @Test
    public void previousSong_null() {
        radio.previousSong(99);
        assertEquals(null, radio.getSong(99));
    }

    @Test
    public void getFrequencyIndex() {
    }

    @Test
    public void getActualRadio() {
    }

    @Test
    public void getActualIndex() {
    }

    @Test
    public void loadPreset() {
    }

    @Test
    public void saveTime() {
    }

    @Test
    public void getPresetsFM() {
    }

    @Test
    public void getPresetsDAB() {
    }

    @Test
    public void getTuner() {
        assertNotNull(radio.getTuner());
    }

    @Test
    public void getTunerFM() {
        assertNotNull(radio.getTunerFM());
    }

    @Test
    public void getBluetooth() {
        assertNotNull(radio.getBluetooth());
    }

    @Test
    public void getAlarm() {
        assertNotNull(radio.getAlarm());
    }

    @Test
    public void getAux() {
        assertNotNull(radio.getAux());
    }

    @Test
    public void getUsb() {
        assertNotNull(radio.getUsb());
    }

    @Test
    public void getClock() {
        assertNotNull(radio.getClock());
    }

    @Test
    public void getSecondSpeaker() {
        assertNotNull(radio.getSecondSpeaker());
    }

    @Test
    public void getOutputState() {
        assertEquals(0, radio.getOutputState());
    }

    @Test
    public void isOutputAuxActivated_true() {
        radio.setOutputAuxActivated(true);
        assertEquals(true, radio.isOutputAuxActivated());
    }
}