package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AutotuneComponentTest {

    AutotuneComponent autotuneComponent = new AutotuneComponent(Radio.getInstance());

    @Test
    public void getInstance() {
        assertNotNull(autotuneComponent);
    }

    @Test
    public void setAutotuneList() {
        assertNotNull(autotuneComponent.setAutotuneList());
    }
}