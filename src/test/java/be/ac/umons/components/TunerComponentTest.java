package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TunerComponentTest{

    TunerComponent tuner = new TunerComponent();

    @Test
    public void nextChannel() {
        tuner.setChannelIndex(0);
        tuner.nextChannel();
        assertEquals("Classic 21", tuner.getActualRadio());
    }

    @Test
    public void nextChannelAtMax() {
        tuner.setChannelIndex(tuner.getChannels().size() - 1);
        System.out.println(tuner.getActualIndex());
        tuner.nextChannel();
        assertEquals("La Première", tuner.getActualRadio());
    }

    @Test
    public void previousChannel() {
        tuner.setChannelIndex(5);
        tuner.previousChannel();
        assertEquals("Bel RTL", tuner.getActualRadio());
    }

    @Test
    public void previousChannelAtMin() {
        tuner.setChannelIndex(0);
        tuner.previousChannel();
        assertEquals("VRT NWS", tuner.getActualRadio());
    }

    @Test
    public void getActualChannel() {
        tuner.setChannelIndex(3);
        assertEquals("5B", tuner.getActualChannel());
    }

    @Test
    public void getActualRadio() {
        tuner.setChannelIndex(3);
        assertEquals("VivaCité Namur", tuner.getActualRadio());
    }

    @Test
    public void getActualIndex() {
    }

    @Test
    public void setChannelIndex() {
        tuner.setChannelIndex(3);
        assertEquals(3, tuner.getActualIndex());
    }
}