package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ClockComponentTest {

    ClockComponent clockComponent = new ClockComponent();

    @Test
    public void getInstance() {
        assertNotNull(clockComponent);
    }

    @Test
    public void isDisplayOn() {
        assertEquals(true, clockComponent.isDisplayOn());
    }

    @Test
    public void setDisplayOn() {
        clockComponent.setDisplayOn(false);
        assertEquals(false, clockComponent.isDisplayOn());
    }

    @Test
    public void getHours() {
        clockComponent.setHours(23);
        assertEquals(23, clockComponent.getHours());
    }

    @Test
    public void setHours() {
        clockComponent.setHours(6);
        assertEquals(6, clockComponent.getHours());
    }

    @Test
    public void getMinutes() {
        clockComponent.setMinutes(59);
        assertEquals(59, clockComponent.getMinutes());
    }

    @Test
    public void setMinutes() {
        clockComponent.setMinutes(1);
        assertEquals(1, clockComponent.getMinutes());
    }

    @Test
    public void getSeconds() {
        clockComponent.setSeconds(50);
        assertEquals(50, clockComponent.getSeconds());
    }

    @Test
    public void setSeconds() {
        clockComponent.setSeconds(30);
        assertEquals(30, clockComponent.getSeconds());
    }

    @Test
    public void getDay() {
        clockComponent.setDay(30);
        assertEquals(30, clockComponent.getDay());
    }

    @Test
    public void setDay() {
        clockComponent.setDay(5);
        assertEquals(5, clockComponent.getDay());
    }

    @Test
    public void getMonth() {
        clockComponent.setMonth(3);
        assertEquals(3, clockComponent.getMonth());
    }

    @Test
    public void setMonth() {
        clockComponent.setMonth(7);
        assertEquals(7, clockComponent.getMonth());
    }

    @Test
    public void getYear() {
        clockComponent.setYear(2020);
        assertEquals(2020, clockComponent.getYear());
    }

    @Test
    public void setYear() {
        clockComponent.setYear(2500);
        assertEquals(2500, clockComponent.getYear());
    }
}