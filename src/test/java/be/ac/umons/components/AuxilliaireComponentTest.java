package be.ac.umons.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AuxilliaireComponentTest {

    AuxilliaireComponent auxilliaireComponent = new AuxilliaireComponent();

    @Test
    public void next() {
        auxilliaireComponent.setIndexSong(3);
        auxilliaireComponent.next();
        assertEquals("Ready the Prince - PB&J", auxilliaireComponent.getActualSong());
    }

    @Test
    public void nextAtMax() {
        auxilliaireComponent.setIndexSong(auxilliaireComponent.getSongs().size() - 1);
        auxilliaireComponent.next();
        assertEquals("Shaka Ponk - Gung Ho", auxilliaireComponent.getActualSong());
    }

    @Test
    public void previous() {
        auxilliaireComponent.setIndexSong(3);
        auxilliaireComponent.previous();
        assertEquals("Sam Fender - The Borders", auxilliaireComponent.getActualSong());
    }

    @Test
    public void previousAtMin() {
        auxilliaireComponent.setIndexSong(0);
        auxilliaireComponent.previous();
        assertEquals("Andy Grammar - Fresh Eyes", auxilliaireComponent.getActualSong());
    }

    @Test
    public void setIndexSong() {
        auxilliaireComponent.setIndexSong(4);
        assertEquals(4, auxilliaireComponent.getIndexSong());
    }
}