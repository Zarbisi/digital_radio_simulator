package be.ac.umons.controllers;

import javafx.animation.KeyFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import java.io.IOException;

public class TimeSettingController extends AbstractClockController {

    // TODO : check after user input for date for leap year, even month

    @FXML
    private Button dateTimeButton;
    private String modifState = "hours";

    @FXML
    public void initialize() {
        super.checkComponents();
        initializeClock();
        super.setEventPresetButton();
        setEventButton(dateTimeButton);
        super.displayTime();
    }

    @Override
    public void handleButtonClick(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        switch(btnClicked.getId()){
            case "powerButton":
                super.turnOffRadio();
                break;
            case "volumeIncreaseButton":
                increaseSelectedValue();
                break;
            case "volumeDecreaseButton":
                decreaseSelectedValue();
                break;
        }
    }

    protected void initializeClock() {
        super.setClockAnimation();
        super.updateTimeAndDateDisplay();
        super.tickingClock.getKeyFrames().add(new KeyFrame(Duration.seconds(1), actionEvent -> {
            if (modifState.equals("hours")) {
                super.displaySecond.setVisible(true);
                super.displayHour.setVisible(!displayHour.isVisible());
            } else if (modifState.equals("minutes")) {
                super.displayHour.setVisible(true);
                super.displayMinute.setVisible(!displayMinute.isVisible());
            } else if (modifState.equals("seconds")) {
                super.displayMinute.setVisible(true);
                super.displaySecond.setVisible(!displaySecond.isVisible());
            } else if (modifState.equals("day")) {
                super.displayDateYear.setVisible(true);
                super.displayDateDay.setVisible(!displayDateDay.isVisible());
            } else if (modifState.equals("month")) {
                super.displayDateDay.setVisible(true);
                super.displayDateMonth.setVisible(!displayDateMonth.isVisible());
            } else if (modifState.equals("year")) {
                super.displayDateMonth.setVisible(true);
                super.displayDateYear.setVisible(!displayDateYear.isVisible());
            }
        }));
        super.tickingClock.play();
    }

    private void setEventButton(Button btn) {
        btn.addEventHandler(MouseEvent.ANY, new EventHandler<>() {
            long startTime;
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    startTime = System.currentTimeMillis();
                } else if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if (System.currentTimeMillis() - startTime > 3* 1000) {
                        try {
                            TimeSettingController.super.loadFXML("NotPlayingPanel");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        switchState();
                    }
                }
            }
        });
    }

    private void increaseSelectedValue() {
        switch (modifState) {
            case "hours" :
                incrementHours();
                break;
            case "minutes" :
                incrementMinutes();
                break;
            case"seconds" :
                incrementSeconds();
                break;
            case "day":
                incrementDay();
                break;
            case "month":
                incrementMonth();
                break;
            case "year":
                incrementYear();
                break;
        }
        super.updateTimeAndDateDisplay();
    }

    private void decreaseSelectedValue() {
        switch (modifState) {
            case "hours" :
                decrementHours();
                break;
            case "minutes" :
                decrementMinutes();
                break;
            case"seconds" :
                decrementSeconds();
                break;
            case "day":
                decrementDay();
                break;
            case "month":
                decrementMonth();
                break;
            case "year":
                decrementYear();
                break;
        }
        super.updateTimeAndDateDisplay();
    }

    private void switchState() {
        switch (modifState) {
            case "hours":
                modifState = "minutes";
                break;
            case "minutes":
                modifState = "seconds";
                break;
            case "seconds":
                modifState = "day";
                TimeSettingController.super.displayDate();
                break;
            case "day":
                modifState = "month";
                break;
            case "month":
                modifState = "year";
                break;
            case "year":
                TimeSettingController.super.displayTime();
                modifState = "hours";
                break;
        }
    }

    private void incrementHours() {
        super.hours.getAndIncrement();
        if (super.hours.get() >= 24) {
            super.hours.set(0);
        }
    }

    private void decrementHours() {
        super.hours.getAndDecrement();
        if (super.hours.get() < 0) {
            super.hours.set(23);
        }
    }

    private void incrementMinutes() {
        super.minutes.getAndIncrement();
        if (super.minutes.get() >= 60) {
            super.minutes.set(0);
        }
    }

    private void decrementMinutes() {
        super.minutes.getAndDecrement();
        if (super.minutes.get() < 0) {
            super.minutes.set(59);
        }
    }

    private void incrementSeconds() {
        super.seconds.getAndIncrement();
        if (super.seconds.get() >= 60) {
            super.seconds.set(0);
        }
    }

    private void decrementSeconds() {
        super.seconds.getAndDecrement();
        if (super.seconds.get() < 0) {
            super.seconds.set(59);
        }
    }

    private void incrementDay() {
        super.day.getAndIncrement();
        if (month.intValue() == 2) {
            if (super.checkLeapYear()){
                if (super.day.get() >= 30){
                    day.set(1);
                }
            } else {
                if (super.day.get() >= 29) {
                    day.set(1);
                }
            }
        }else if (month.intValue()%2 == 0) {
            if (super.day.get() >= 31) {
                super.day.set(1);
            }
        } else {
            if (super.day.get() >= 32) {
                super.day.set(1);
            }
        }
    }

    private void decrementDay() {
        super.day.getAndDecrement();
        if (super.day.get() <= 0){
            if (super.month.intValue()%2 == 0) {
                day.set(30);
            } else {
                super.day.set(31);
            }
        }
    }

    private void incrementMonth() {
        super.month.getAndIncrement();
        if (super.month.get() >= 13) {
            super.month.set(1);
        }
    }

    private void decrementMonth() {
        super.month.getAndDecrement();
        if (super.month.get() <= 0) {
            super.month.set(12);
        }
    }

    private void incrementYear() {
        super.year.getAndIncrement();
        if (super.year.get() >= 3000) {
            super. year.set(2000);
        }
    }

    private void decrementYear() {
        super.year.getAndDecrement();
        if (super.year.get() <= 1999) {
            super.year.set(2999);
        }
    }
}
