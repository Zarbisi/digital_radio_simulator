package be.ac.umons.controllers;

import be.ac.umons.App;
import be.ac.umons.enumerations.ComponentStatus;
import be.ac.umons.models.RadioDABStation;
import be.ac.umons.models.RadioStation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPlayingRadioController extends AbstractPlayingController {

    public ListView bluetoothDeviceListing;
    public ListView autotuneList;
    private List<String> bluetoothDeviceList = new ArrayList();

    protected void checkComponents(int idController) {
        super.initializeRadio();
        linkFXMLElements();
        setBluetoothDevice();
        super.checkComponents();
        super.checkBluetoothIconDisplay();
        activateBreakingNewsOption();
        for (int i = 0; i < super.radio.getNumberPresets(); i ++) {
            String name = "presetButton" +(i+1);
            Button btn = (Button) App.getFxmlLoader().getNamespace().get(name);
            btn.setVisible(true);
            setEventPresetButton(btn, idController);
            super.radio.getPresets(idController).add(i, -99);
        }
    }

    @Override
    protected void linkFXMLElements() {
        super.linkFXMLElements();
        bluetoothDeviceListing = (ListView) App.getFxmlLoader().getNamespace().get("bluetoothDeviceListing");
        autotuneList = (ListView) App.getFxmlLoader().getNamespace().get("autotuneList");
    }

    protected void setEventPresetButton(Button btn, int idController) {
        btn.addEventHandler(MouseEvent.ANY, new EventHandler<>() {
            int idPreset = (Integer.parseInt(btn.getId().replace("presetButton", ""))) - 1 ;
            long startTime;
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    startTime = System.currentTimeMillis();
                } else if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if (System.currentTimeMillis() - startTime < 3* 1000) {
                        if (autotuneList.isVisible()) {
                            int stationToLoad = 0;
                            if (idController == 0 && autotuneList.getSelectionModel().getSelectedItem() != null){
                                stationToLoad = AbstractPlayingRadioController.super.radio.getTuner().getCorrespondingIndex( (RadioDABStation) autotuneList.getSelectionModel().getSelectedItem());
                            } else if (autotuneList.getSelectionModel().getSelectedItem() != null){
                                stationToLoad = AbstractPlayingRadioController.super.radio.getTunerFM().getCorrespondingIndex( (RadioStation) autotuneList.getSelectionModel().getSelectedItem());
                            }
                            AbstractPlayingRadioController.super.radio.getPresets(idController).add(idPreset, stationToLoad);
                        }
                        else if (AbstractPlayingRadioController.super.radio.getPresets(idController).get(idPreset) != null && radio.getPresets(idController).get(idPreset) != -99) {
                            AbstractPlayingRadioController.super.radio.loadPreset(idPreset, idController);
                            checkStateDisplay(idController);
                        }
                    } else {
                        AbstractPlayingRadioController.super.radio.getPresets(idController).add(idPreset, AbstractPlayingRadioController.super.radio.getActualIndex());
                    }
                }
            }
        });
    }
    @Override
    protected void checkStateDisplay(int id) {
        String radioName = null;
        if (id == 0) {
            displayStation.setText( super.radio.getActualChannel());
        } else {
            displayStation.setText( super.radio.getActualFrequency());
        }
        radioName =  super.radio.getActualRadio(id);
        if (radioName != null) {
            displayStation.setText(radioName + " - " + displayStation.getText());
        }

        if (id == 0 && super.radio.getTuner().isFailing() || id == 1 && super.radio.getTunerFM().isFailing()) {
            displayStation.setText("No Signal");
            displaySong.setVisible(false);
        }
    }

    protected void activateBreakingNewsOption() {
        if (radio.getBreakingNews().getStatus() == ComponentStatus.FUNCTIONAL) {
            MenuItem breakingNews = (MenuItem) App.getFxmlLoader().getNamespace().get("breakingNews");
            breakingNews.setVisible(true);
        }
    }

    protected void setBluetoothDevice() {
        bluetoothDeviceList.add("Device 1");
        bluetoothDeviceList.add("Device 2");
        bluetoothDeviceList.add("Device 3");
        ObservableList observableList = FXCollections.observableArrayList();
        observableList.setAll(bluetoothDeviceList);
        bluetoothDeviceListing.setItems(observableList);
    }

    public void broadcastBreakingNews() {
        if (!super.radio.getBreakingNews().isFailing()) {
            super.displayMessage(super.radio.broadcastNews(), 5);
        }
    }

    protected void changeOutput(int id) {
        Circle cablePlugged = (Circle) App.getFxmlLoader().getNamespace().get("cablePlugged");
        if (super.radio.getOutputState() < 1 && super.radio.getOutputBluetoothStatus() != ComponentStatus.NONACTIVATED) {
            cablePlugged.setVisible(true);
            super.radio.setOutputState(1);
            super.radio.setOutputBluetoothActivated(true);
            if (super.radio.getOutputBluetoothStatus() == ComponentStatus.FAILURE) {
                displayStation.setText("No Bluetooth Device Detected");
                displaySong.setVisible(false);
            } else {
                    bluetoothDeviceListing.setVisible(true);
                    displayStation.setText("Select a device for pairing :");
            }
        } else if (super.radio.getOutputState() < 2 && super.radio.getOutputAuxStatus() != ComponentStatus.NONACTIVATED) {
            super.radio.setOutputState(2);
            super.radio.setOutputBluetoothActivated(false);
            super.radio.setOutputAuxActivated(true);
            cablePlugged.setVisible(true);
            if (super.radio.getOutputAuxStatus() == ComponentStatus.FAILURE) {
                displayStation.setText("No Jack cable detected");
                displaySong.setVisible(false);
            }
            checkStateDisplay(id);
        } else {
            cablePlugged.setVisible(false);
            super.radio.setOutputState(0);
            super.radio.setOutputBluetoothActivated(false);
            super.radio.setOutputAuxActivated(false);
            checkStateDisplay(id);
        }
        super.checkBluetoothIconDisplay();
    }

    protected void displayAutotuneList() {
        if (autotuneList.isVisible()) {
            autotuneList.setVisible(false);
        } else {
            autotuneList.setVisible(true);
        }
    }

    @Override
    protected void increaseFrequency(int id){
        if (id == 0 && !super.radio.getTuner().isFailing()){
            super.radio.nextChannel();
        } else if (id == 1 && !super.radio.getTunerFM().isFailing()) {
            super.radio.increaseFrequency();
        }
        checkStateDisplay(id);
    }

    @Override
    protected void decreaseFrequency(int id){
        if (id == 0 && !super.radio.getTuner().isFailing()){
            super.radio.previousChannel();
        } else if (id == 1 && !super.radio.getTunerFM().isFailing()){
            super.radio.decreaseFrequency();
        }
        checkStateDisplay(id);
    }
}
