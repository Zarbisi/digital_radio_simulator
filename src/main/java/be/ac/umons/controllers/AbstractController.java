package be.ac.umons.controllers;

import be.ac.umons.App;
import be.ac.umons.components.Radio;
import be.ac.umons.enumerations.ComponentStatus;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractController {
    protected Radio radio;

    protected Timeline tickingClock;
    protected AtomicInteger hours;
    protected AtomicInteger minutes;
    protected AtomicInteger seconds;
    protected AtomicInteger day;
    protected AtomicInteger month;
    protected AtomicInteger year;
    protected DateTimeFormatter timeDisplay = DateTimeFormatter.ofPattern("HH:mm:ss");
    protected DateTimeFormatter dateDisplay = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public Text displayStation;
    public Text displaySong;
    public Text displayHour;
    public Text displayDate;
    public Text displayMessage;

    public void loadFXML(String fxml) throws IOException {
        saveTimeAndDate();
        if (tickingClock != null && tickingClock.getStatus() == Timeline.Status.RUNNING) {
            tickingClock.stop();
        }
        App.setRoot(fxml);
    };

    protected void initializeRadio() {
        radio = Radio.getInstance();
        linkFXMLElements();
    }

    protected void checkComponents() {
        checkAlarmIconDisplay();
        checkUIFailures();

        if (radio.getSecondSpeaker().getStatus() != ComponentStatus.NONACTIVATED) {
            Rectangle secondSpeaker = (Rectangle) App.getFxmlLoader().getNamespace().get("secondSpeaker");
            secondSpeaker.setVisible(true);

            Group balanceGroup = (Group) App.getFxmlLoader().getNamespace().get("balanceGroup");
            balanceGroup.setVisible(true);
        }

        if (radio.getAlarm().getStatus() != ComponentStatus.NONACTIVATED) {
            Button alarmButton = (Button) App.getFxmlLoader().getNamespace().get("alarmButton");
            alarmButton.setVisible(true);
        }

        if (radio.getAutotune().getStatus() != ComponentStatus.NONACTIVATED) {
            Button autotuneButton = (Button) App.getFxmlLoader().getNamespace().get("autotuneButton");
            autotuneButton.setVisible(true);
        }

        if (radio.getUsb().getStatus() != ComponentStatus.NONACTIVATED){
            Group InputUSB = (Group) App.getFxmlLoader().getNamespace().get("USBInput");
            InputUSB.setVisible(true);
        }

        if (radio.getOutputAuxStatus() != ComponentStatus.NONACTIVATED || radio.getAux().getStatus() != ComponentStatus.NONACTIVATED) {
            Group inputAUX = (Group) App.getFxmlLoader().getNamespace().get("inputAUX");
            inputAUX.setVisible(true);
        }

        if (radio.getBluetooth().getStatus() != ComponentStatus.NONACTIVATED || radio.getAux().getStatus() != ComponentStatus.NONACTIVATED
        || radio.getUsb().getStatus() != ComponentStatus.NONACTIVATED || radio.getTunerFM().getStatus() != ComponentStatus.NONACTIVATED) {
            Button inputButton = (Button) App.getFxmlLoader().getNamespace().get("inputButton");
            inputButton.setVisible(true);
        }

        if (radio.getOutputBluetoothStatus() != ComponentStatus.NONACTIVATED || radio.getOutputAuxStatus() != ComponentStatus.NONACTIVATED) {
            Button outputButton = (Button) App.getFxmlLoader().getNamespace().get("outputButton");
            outputButton.setVisible(true);
        }

        if (radio.getOutputBluetoothStatus() != ComponentStatus.NONACTIVATED
                || radio.getAutotune().getStatus() != ComponentStatus.NONACTIVATED
                || radio.getAutotuneFM().getStatus() != ComponentStatus.NONACTIVATED) {
            Button confirmButton = (Button) App.getFxmlLoader().getNamespace().get("confirmButton");
            confirmButton.setVisible(true);
        }
    }

    public abstract void handleButtonClick(ActionEvent event);

    private void initializeClock() {
        this.tickingClock = new Timeline();
        this.tickingClock.setCycleCount(Animation.INDEFINITE);
        hours = new AtomicInteger(radio.getClock().getHours());
        minutes = new AtomicInteger(radio.getClock().getMinutes());
        seconds = new AtomicInteger(radio.getClock().getSeconds());
        day = new AtomicInteger(radio.getClock().getDay());
        month = new AtomicInteger(radio.getClock().getMonth());
        year = new AtomicInteger(radio.getClock().getYear());
    }

    protected void displayMessage(String message , int duration) {
        this.displayStation.setVisible(false);
        if (displaySong != null){
            this.displaySong.setVisible(false);
        }
        this.displayHour.setVisible(false);
        this.displayDate.setVisible(false);
        this.displayMessage.setText(message);
        this.displayMessage.setVisible(true);

        PauseTransition wait = new PauseTransition(Duration.seconds(duration));
        wait.setOnFinished((e) -> {
            this.displayMessage.setVisible(false);
            this.displayStation.setVisible(true);
            if (displaySong != null) {
                this.displaySong.setVisible(true);
            }
            this.displayHour.setVisible(true);
            this.displayDate.setVisible(true);
        });
        wait.play();
    }

    protected void setClockAnimation() {
        initializeClock();
        updateTimeAndDateDisplay();

        if (!radio.getClock().isAutomatic()) {
            String currentTime = LocalTime.of(hours.intValue(), minutes.intValue()).format(timeDisplay);
            displayHour.setText(currentTime);
            tickingClock.getKeyFrames().add(new KeyFrame(Duration.seconds(1), actionEvent -> {
                augmentSecond();
                updateTimeAndDateDisplay();
                checkAlarm();
            }));
        } else {
            tickingClock.getKeyFrames().add(new KeyFrame(Duration.seconds(1), actionEvent -> {
                String currentTime = LocalTime.now().format(timeDisplay);
                if (!App.getFxmlLoader().getController().toString().contains("TimeSettingController")
                        && !App.getFxmlLoader().getController().toString().contains("AlarmController")) {
                    displayHour.setText(currentTime);
                }
                hours.set(LocalTime.now().getHour());
                minutes.set(LocalTime.now().getMinute());
                seconds.set(LocalTime.now().getSecond());
                String currentDate = LocalDate.now().format(dateDisplay);
                if (!App.getFxmlLoader().getController().toString().contains("TimeSettingController")
                && !App.getFxmlLoader().getController().toString().contains("AlarmController")) {
                    displayDate.setText(currentDate);
                }
                day.set(LocalDate.now().getDayOfMonth());
                month.set(LocalDate.now().getMonthValue());
                year.set(LocalDate.now().getYear());
                checkAlarm();
            }));
        }
    }

    protected void checkAlarm() {
        if (radio.getAlarm().getStatus() == ComponentStatus.FUNCTIONAL && radio.isAlarmActivated()) {
            if (radio.getAlarmHour() == hours.intValue()) {
                if (radio.getAlarmMinute() == minutes.intValue()) {
                    try {
                        loadFXML("AlarmPanel");
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    protected void saveTimeAndDate() {
        if (!radio.getClock().isFailing()) {
            if (hours != null && minutes != null && seconds != null) {
                radio.saveTime(hours.intValue(), minutes.intValue(), seconds.intValue());
            }

            if (day != null && month != null && year != null) {
                radio.saveDate(day.intValue(), month.intValue(), year.intValue());
            }
        } else {
            radio.saveTime(0, 0, 0);
            radio.saveDate(10, 9, 2019);
        }
    }

    protected void checkAlarmIconDisplay() {
        ImageView alarmPicture = (ImageView) App.getFxmlLoader().getNamespace().get("alarmPicture");
        if (radio.isAlarmActivated()) {
            alarmPicture.setVisible(true);
        } else {
            alarmPicture.setVisible(false);
        }
    }

    protected void checkBluetoothIconDisplay() {
        ImageView bluetoothPicture = (ImageView) App.getFxmlLoader().getNamespace().get("bluetoothPicture");
        if (radio.isOutputBluetoothActivated()) {
            bluetoothPicture.setVisible(true);
        } else {
            bluetoothPicture.setVisible(false);
        }
    }

    protected void updateTimeAndDateDisplay() {
        String newTime = LocalTime.of(hours.intValue(), minutes.intValue(), seconds.intValue()).format(timeDisplay);
        this.displayHour.setText(newTime);
        String newDate = LocalDate.of(year.intValue(), month.intValue(), day.intValue()).format(dateDisplay);
        this.displayDate.setText(newDate);
    }

    protected void linkFXMLElements() {
        displayStation = (Text) App.getFxmlLoader().getNamespace().get("displayStation");
        displaySong = (Text) App.getFxmlLoader().getNamespace().get("displaySong");
        displayHour = (Text) App.getFxmlLoader().getNamespace().get("displayHour");
        displayDate = (Text) App.getFxmlLoader().getNamespace().get("displayDate");
        displayMessage = (Text) App.getFxmlLoader().getNamespace().get("displayMessage");
    }

    protected void switchOnOffAlarm() {
        if (!radio.getAlarm().isFailing()) {
            radio.toggleAlarm();
            checkAlarmIconDisplay();
        }
    }

    protected void setEventAlarmSetting() {
        Button btn = (Button) App.getFxmlLoader().getNamespace().get("alarmButton");
        btn.addEventHandler(MouseEvent.ANY, new EventHandler<>() {
            long startTime;
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    startTime = System.currentTimeMillis();
                } else if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if (System.currentTimeMillis() - startTime > 3* 1000) {
                        try {
                            loadFXML("AlarmSettingPanel");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        switchOnOffAlarm();
                    }
                }
            }
        });
    }

    public void switchBackToConfig() throws IOException {
        loadFXML("ConfigurationPanel");
    }

    protected void augmentSecond() {
        seconds.getAndIncrement();
        if (seconds.get() >= 60){
            augmentMinute();
        }
    }

    private void augmentMinute() {
        seconds.getAndSet(0);
        minutes.getAndIncrement();
        if (minutes.get() >= 60){
            augmentHour();
        }
    }

    private void augmentHour() {
        minutes.getAndSet(0);
        hours.getAndIncrement();
        if (hours.get() >= 24) {
            augmentDay();
        }
    }

    private void augmentDay() {
        hours.getAndSet(0);
        day.getAndIncrement();
        if (month.intValue() == 2 ) {
            if (checkLeapYear()) {
                if (day.get() >= 30) {
                    augmentMonth();
                }
            } else {
                if (day.get() >= 29){
                    augmentMonth();
                }
            }
        } else if (month.intValue()%2 == 0){
            if (day.get() >= 31) {
                augmentMonth();
            }
        } else {
            if (day.get() >= 32) {
                augmentMonth();
            }
        }
    }

    private void augmentMonth() {
        day.set(1);
        month.getAndIncrement();
        if (month.intValue() >= 13) {
            augmentYear();
        }
    }

    private void augmentYear() {
        month.set(1);
        if (year.get() >= 3000) {
            year.set(2000);
        }
    }

    protected boolean checkLeapYear() {
        if (year.intValue()%4 == 0) {
            return true;
        } else {
            return false;
        }
    }

    protected void setEventPresetButton() {
        for (int i = 0; i < radio.getNumberPresets(); i ++) {
            String name = "presetButton" + (i + 1);
            Button btn = (Button) App.getFxmlLoader().getNamespace().get(name);
            btn.setVisible(true);
        }
    }

    protected void startClock() {
        tickingClock.play();
    }

    private void checkUIFailures() {
        HashSet<String> visualFailures = new HashSet<>();
        visualFailures = radio.getFailureIdsUI();

        if (radio.getPower().getStatus() == ComponentStatus.FAILURE) {
            hideDisplay();
        }

        for (String failure : visualFailures) {
            if (failure.contains("Button")) {
                Button btn = (Button) App.getFxmlLoader().getNamespace().get(failure);
                btn.setDisable(true);
            } else if (failure.contains("Group")) {
                Group grp = (Group) App.getFxmlLoader().getNamespace().get(failure);
                grp.setDisable(true);
            }
            else if (failure.equals("display")) {
                hideDisplay();
            }
        }
    }

    private void hideDisplay() {
        Rectangle display = (Rectangle) App.getFxmlLoader().getNamespace().get("display");
        display.setFill(Color.GRAY);
        Group displayGroup = (Group) App.getFxmlLoader().getNamespace().get("displayGroup");
        displayGroup.setVisible(false);
    }

    protected void turnOffRadio() {
        if (radio.getPower().getStatus() != ComponentStatus.FAILURE){
            try {
                loadFXML("NotPlayingPanel");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
