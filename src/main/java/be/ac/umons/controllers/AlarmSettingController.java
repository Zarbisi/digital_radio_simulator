package be.ac.umons.controllers;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class AlarmSettingController extends AbstractClockController {

    @FXML
    private Button alarmButton;
    private AtomicInteger alarmHour;
    private AtomicInteger alarmMinute;
    private String modifState = "hour";

    @FXML
    public void initialize() {
        super.checkComponents();
        initializeClock();
        super.setEventPresetButton();
        setEventAlarmButton(alarmButton);
    }

    @Override
    public void handleButtonClick(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        if (btnClicked.getId().equals("powerButton")) {
            super.turnOffRadio();
        }
        if (btnClicked.getId().equals("volumeIncreaseButton")) {
            switch(modifState) {
                case "hour":
                    increaseHour();
                    break;
                case "minute":
                    increaseMinute();
                    break;
            }
            updateClock();
        }
        if (btnClicked.getId().equals("volumeDecreaseButton")) {
            switch(modifState) {
                case "hour":
                    decreaseHour();
                    break;
                case "minute":
                    decreaseMinute();
                    break;
            }
            updateClock();
        }
        if (btnClicked.getId().equals("dateTimeButton")) {
            alarmSoundSetting();
        }
    }

    private void initializeClock() {
        super.tickingClock = new Timeline();
        super.tickingClock.setCycleCount(Animation.INDEFINITE);
        alarmHour = new AtomicInteger(super.radio.getAlarmHour());
        alarmMinute = new AtomicInteger(super.radio.getAlarmMinute());
        updateClock();
        super.tickingClock.getKeyFrames().add(new KeyFrame(Duration.seconds(1), actionEvent -> {
            if (modifState.equals("hour")) {
                super.displayMinute.setVisible(true);
                super.displayHour.setVisible(!displayHour.isVisible());
            } else if (modifState.equals("minute")) {
                super.displayHour.setVisible(true);
                super.displayMinute.setVisible(!displayMinute.isVisible());
            }
        }));
        super.tickingClock.play();
    }

    private void setEventAlarmButton(Button btn) {
        btn.addEventHandler(MouseEvent.ANY, new EventHandler<>() {
            long startTime;
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    startTime = System.currentTimeMillis();
                } else if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if (System.currentTimeMillis() - startTime > 3* 1000) {
                        AlarmSettingController.super.radio.setAlarmHour(alarmHour.intValue());
                        AlarmSettingController.super.radio.setAlarmMinute(alarmMinute.intValue());
                        try {
                            AlarmSettingController.super.loadFXML("NotPlayingPanel");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        switchState();
                    }
                }
            }
        });
    }

    private void switchState() {
        switch (modifState) {
            case "hour":
                modifState = "minute";
                break;
            case "minute":
                modifState = "hour";
                break;
        }
    }

    private void increaseHour() {
        alarmHour.getAndIncrement();
        if (alarmHour.get() >= 24) {
            alarmHour.set(0);
        }
    }

    private void decreaseHour() {
        alarmHour.getAndDecrement();
        if (alarmHour.get() < 0) {
            alarmHour.set(23);
        }
    }

    private void increaseMinute() {
        alarmMinute.getAndIncrement();
        if (alarmMinute.get() >= 60) {
            alarmMinute.set(0);
        }
    }

    private void decreaseMinute() {
        alarmMinute.getAndDecrement();
        if (alarmMinute.get() < 0) {
            alarmMinute.set(59);
        }
    }

    private void alarmSoundSetting() {
        if (super.radio.checkAlarmSound()) {
            super.displayMessage("Radio Station", 1);
            super.radio.setAlarmSound(false);
        } else {
            super.displayMessage("Alarm Sound", 1);
            super.radio.setAlarmSound(true);
        }
    }

    private void updateClock() {
        super.displayHour.setText(super.decimalFormat.format(alarmHour.intValue()));
        super.displayMinute.setText(":" + super.decimalFormat.format(alarmMinute.intValue()));
    }
}
