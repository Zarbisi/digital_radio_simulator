package be.ac.umons.controllers;

import be.ac.umons.enumerations.ComponentStatus;
import be.ac.umons.models.RadioStation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;

public class PlayingFMControllerPlaying extends AbstractPlayingRadioController {

    private int idController = 1;
    private ObservableList observableAutotuneList = FXCollections.observableArrayList();

    @FXML
    public void initialize() {
        super.checkComponents(idController);
        super.setClockAnimation();
        super.startClock();
        super.checkStateDisplay(idController);
        super.setEventAlarmSetting();
        super.displayMessage("FM", 2);
    }

    public void handleButtonClick(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        switch(btnClicked.getId()) {
            case "dateTimeButton":
                super.switchToDateHourMode();
                break;
            case "powerButton":
                super.turnOffRadio();
                break;
            case "volumeIncreaseButton":
                super.displayMessage("Volume " + super.radio.increaseVolume(), 1);
                break;
            case "volumeDecreaseButton":
                super.displayMessage("Volume " + super.radio.decreaseVolume(), 1);
                break;
            case "balanceIncreaseButton":
                super.displayMessage("Balance " + super.radio.increaseBalance(), 1);
                break;
            case "balanceDecreaseButton":
                super.displayMessage("Balance " + super.radio.decreaseBalance(), 1);
                break;
            case "frequencyIncreaseButton":
                super.increaseFrequency(idController);
                break;
            case "frequencyDecreaseButton":
                super.decreaseFrequency(idController);
                break;
            case "inputButton":
                super.changeInput(idController);
                break;
            case "outputButton":
                super.changeOutput(idController);
                break;
            case "autotuneButton":
                if (super.radio.getAutotuneFM().getStatus() == ComponentStatus.FUNCTIONAL && !super.radio.getTunerFM().isFailing()) {
                    setAutotuneStations();
                    super.displayAutotuneList();
                }
                break;
            case "confirmButton":
                if (super.radio.getOutputBluetoothStatus() == ComponentStatus.FUNCTIONAL && super.bluetoothDeviceListing.isVisible()
                        && super.bluetoothDeviceListing.getSelectionModel().getSelectedItem() != null){
                    super.radio.setOutputBluetoothActivated(true);
                    super.bluetoothDeviceListing.setVisible(false);
                    super.checkBluetoothIconDisplay();
                    checkStateDisplay(idController);
                    super.displayMessage("Pairing with " + super.bluetoothDeviceListing.getSelectionModel().getSelectedItem() + " completed", 3);
                } else if (super.radio.getAutotune().getStatus() == ComponentStatus.FUNCTIONAL && super.autotuneList.isVisible()
                        && super.autotuneList.getSelectionModel().getSelectedItem() != null) {
                    int stationToLoad = super.radio.getTunerFM().getCorrespondingIndex( (RadioStation) super.autotuneList.getSelectionModel().getSelectedItem());
                    super.radio.getTunerFM().setFrequencyIndex(stationToLoad);
                    super.checkStateDisplay(idController);
                    super.autotuneList.setVisible(false);
                }
                break;
        }
    }

    private void setAutotuneStations() {
        super.autotuneList.setCellFactory(autotuneList -> {
            return new ListCell<RadioStation>() {
                @Override
                protected void updateItem(RadioStation item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null) {
                        setText(null);
                    } else {
                        setText(item.getFrequency() + " - " + item.getName());
                    }
                }
            };
        });

        observableAutotuneList.setAll(super.radio.getAutotuneFM().setAutotuneList());
        super.autotuneList.setItems(observableAutotuneList);
    }
}
