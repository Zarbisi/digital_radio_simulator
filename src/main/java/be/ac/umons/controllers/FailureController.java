package be.ac.umons.controllers;

import be.ac.umons.App;
import be.ac.umons.enumerations.ComponentStatus;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;

import java.io.IOException;
import java.util.HashSet;

public class FailureController extends AbstractController {

    /* Base Components Failure */
    @FXML
    private CheckBox activationFailure;
    @FXML
    private CheckBox tunerDABFailure;
    @FXML
    private CheckBox mainSpeakerFailure;
    @FXML
    private CheckBox clockBatteryFailure;
    //------------------------------------//
    @FXML
    private CheckBox autotuneDABFailure;
    @FXML
    private CheckBox autotuneFMFailure;
    @FXML
    private CheckBox secondSpeakerFailure;
    @FXML
    private CheckBox inputBluetoothFailure;
    @FXML
    private CheckBox inputAuxFailure;
    @FXML
    private CheckBox inputUsbFailure;
    @FXML
    private CheckBox outputBluetoothFailure;
    @FXML
    private CheckBox outputAuxFailure;
    @FXML
    private CheckBox alarmFailure;
    @FXML
    private CheckBox breakingNewsFailure;
    @FXML
    private CheckBox tunerFmFailure;
    /**/

    /* Visual Components Failure */

    @FXML
    private CheckBox displayScreenFailure;
    @FXML
    private CheckBox volumeButtonFailure;
    @FXML
    private CheckBox stationButtonFailure;
    @FXML
    private CheckBox confirmButtonFailure;
    @FXML
    private CheckBox clockButtonFailure;
    @FXML
    private CheckBox powerButtonFailure;
    @FXML
    private CheckBox preset1ButtonFailure;
    @FXML
    private CheckBox preset2ButtonFailure;
    @FXML
    private CheckBox preset3ButtonFailure;
    //------------------------------------//
    @FXML
    private CheckBox balanceButtonFailure;
    @FXML
    private CheckBox autotuneButtonFailure;
    @FXML
    private CheckBox alarmButtonFailure;
    @FXML
    private CheckBox inputButtonFailure;
    @FXML
    private CheckBox outputButtonFailure;
    @FXML
    private CheckBox preset4ButtonFailure;
    @FXML
    private CheckBox preset5ButtonFailure;
    @FXML
    private CheckBox preset6ButtonFailure;
    @FXML
    private CheckBox preset7ButtonFailure;
    @FXML
    private CheckBox preset8ButtonFailure;
    @FXML
    private CheckBox preset9ButtonFailure;
    @FXML
    private CheckBox preset10ButtonFailure;
    /**/

    HashSet<String> failureList = new HashSet<>();

    @FXML
    void initialize() {
        checkComponents();
        setComponentsFailure();
    }

    public void switchToConfigurationPanel() throws IOException {
        setComponentsFailure();
        App.setRoot("ConfigurationPanel");
    }

    @Override
    public void checkComponents() {
        super.initializeRadio();

        if (super.radio.getPower().getStatus() == ComponentStatus.FUNCTIONAL) {
            activationFailure.setSelected(false);
        } else if (super.radio.getPower().getStatus() == ComponentStatus.FAILURE) {
            activationFailure.setSelected(true);
        }

        if (super.radio.getTuner().getStatus() == ComponentStatus.FUNCTIONAL) {
            tunerDABFailure.setSelected(false);
        } else if (super.radio.getTuner().getStatus() == ComponentStatus.FAILURE) {
            tunerDABFailure.setSelected(true);
        }

        if (super.radio.getSpeaker().getStatus() == ComponentStatus.FUNCTIONAL) {
            mainSpeakerFailure.setSelected(false);
        } else if (super.radio.getSpeaker().getStatus() == ComponentStatus.FAILURE) {
            mainSpeakerFailure.setSelected(true);
        }

        if (super.radio.getClock().getStatus() == ComponentStatus.FUNCTIONAL) {
            clockBatteryFailure.setSelected(false);
        } else if (super.radio.getClock().getStatus() == ComponentStatus.FAILURE) {
            clockBatteryFailure.setSelected(true);
        }

        //--------------------------------------------------------------------------------------//

        if (super.radio.getAutotune().getStatus() == ComponentStatus.FUNCTIONAL){
            autotuneDABFailure.setDisable(false);
        }else if (super.radio.getAutotune().getStatus() == ComponentStatus.FAILURE){
            autotuneDABFailure.setDisable(false);
            autotuneDABFailure.setSelected(true);
        }

        if (super.radio.getAutotuneFM().getStatus() == ComponentStatus.FUNCTIONAL){
            autotuneFMFailure.setDisable(false);
        }else if (super.radio.getAutotuneFM().getStatus() == ComponentStatus.FAILURE){
            autotuneFMFailure.setDisable(false);
            autotuneFMFailure.setSelected(true);
        }

        if (super.radio.getSecondSpeaker().getStatus() == ComponentStatus.FUNCTIONAL){
            secondSpeakerFailure.setDisable(false);
        }else if (super.radio.getSecondSpeaker().getStatus() == ComponentStatus.FAILURE){
            secondSpeakerFailure.setDisable(false);
            secondSpeakerFailure.setSelected(true);
        }

        if (super.radio.getBluetooth().getStatus() == ComponentStatus.FUNCTIONAL){
            inputBluetoothFailure.setDisable(false);
        }else if (super.radio.getBluetooth().getStatus() == ComponentStatus.FAILURE){
            inputBluetoothFailure.setDisable(false);
            inputBluetoothFailure.setSelected(true);
        }

        if (super.radio.getAux().getStatus() == ComponentStatus.FUNCTIONAL){
            inputAuxFailure.setDisable(false);
        }else if (super.radio.getAux().getStatus() == ComponentStatus.FAILURE){
            inputAuxFailure.setDisable(false);
            inputAuxFailure.setSelected(true);
        }

        if (super.radio.getUsb().getStatus() == ComponentStatus.FUNCTIONAL){
            inputUsbFailure.setDisable(false);
        }else if (super.radio.getUsb().getStatus() == ComponentStatus.FAILURE){
            inputUsbFailure.setDisable(false);
            inputUsbFailure.setSelected(true);
        }

        if (super.radio.getOutputBluetoothStatus() == ComponentStatus.FUNCTIONAL){
            outputBluetoothFailure.setDisable(false);
        }else if (super.radio.getOutputBluetoothStatus() == ComponentStatus.FAILURE){
            outputBluetoothFailure.setDisable(false);
            outputBluetoothFailure.setSelected(true);
        }

        if (super.radio.getOutputAuxStatus() == ComponentStatus.FUNCTIONAL){
            outputAuxFailure.setDisable(false);
        }else if (super.radio.getOutputAuxStatus() == ComponentStatus.FAILURE){
            outputAuxFailure.setDisable(false);
            outputAuxFailure.setSelected(true);
        }

        if (super.radio.getAlarm().getStatus() == ComponentStatus.FUNCTIONAL){
            alarmFailure.setDisable(false);
        }else if (super.radio.getAlarm().getStatus() == ComponentStatus.FAILURE){
            alarmFailure.setDisable(false);
            alarmFailure.setSelected(true);
        }

        if (super.radio.getBreakingNews().getStatus() == ComponentStatus.FUNCTIONAL){
            breakingNewsFailure.setDisable(false);
        }else if (super.radio.getBreakingNews().getStatus() == ComponentStatus.FAILURE){
            breakingNewsFailure.setDisable(false);
            breakingNewsFailure.setSelected(true);
        }

        if (super.radio.getTunerFM().getStatus() == ComponentStatus.FUNCTIONAL){
            tunerFmFailure.setDisable(false);
        }else if (super.radio.getTunerFM().getStatus() == ComponentStatus.FAILURE){
            tunerFmFailure.setDisable(false);
            tunerFmFailure.setSelected(true);
        }

        /* Visual Failures */

        failureList = super.radio.getFailureIdsUI();

        if (failureList.contains("display")) {
            displayScreenFailure.setSelected(true);
        } else {
            displayScreenFailure.setSelected(false);
        }

        if (failureList.contains("volumeGroup")) {
            volumeButtonFailure.setSelected(true);
        } else {
            volumeButtonFailure.setSelected(false);
        }

        if (failureList.contains("selectionGroup")) {
            stationButtonFailure.setSelected(true);
        } else {
            stationButtonFailure.setSelected(false);
        }

        if (failureList.contains("confirmButton")) {
            confirmButtonFailure.setSelected(true);
        } else {
            confirmButtonFailure.setSelected(false);
        }

        if (failureList.contains("dateTimeButton")) {
            clockButtonFailure.setSelected(true);
        } else {
            clockButtonFailure.setSelected(false);
        }

        if (failureList.contains("powerButton")) {
            powerButtonFailure.setSelected(true);
        } else {
            powerButtonFailure.setSelected(false);
        }

        if (failureList.contains("presetButton1")) {
            preset1ButtonFailure.setSelected(true);
        } else {
            preset1ButtonFailure.setSelected(false);
        }

        if (failureList.contains("presetButton2")) {
            preset2ButtonFailure.setSelected(true);
        } else {
            preset2ButtonFailure.setSelected(false);
        }

        if (failureList.contains("presetButton3")) {
            preset3ButtonFailure.setSelected(true);
        } else {
            preset3ButtonFailure.setSelected(false);
        }

        //-------------------------------------------------------------------//
        if (super.radio.getSecondSpeaker().getStatus() != ComponentStatus.NONACTIVATED) {
            balanceButtonFailure.setDisable(false);
            if (failureList.contains("balanceGroup")) {
                balanceButtonFailure.setSelected(true);
            } else {
                balanceButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getAutotune().getStatus() != ComponentStatus.NONACTIVATED) {
            autotuneButtonFailure.setDisable(false);
            if (failureList.contains("autotuneButton")) {
                autotuneButtonFailure.setSelected(true);
            } else {
                autotuneButtonFailure.setSelected(false);
            }
        }
        if (super.radio.getAlarm().getStatus() != ComponentStatus.NONACTIVATED) {
            alarmButtonFailure.setDisable(false);
            if (failureList.contains("alarmButton")) {
                alarmButtonFailure.setSelected(true);
            } else {
                alarmButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getBluetooth().getStatus() != ComponentStatus.NONACTIVATED ||
                super.radio.getUsb().getStatus() != ComponentStatus.NONACTIVATED ||
                super.radio.getAux().getStatus() != ComponentStatus.NONACTIVATED) {
            inputButtonFailure.setDisable(false);
            if (failureList.contains("inputButton")) {
                inputButtonFailure.setSelected(true);
            } else {
                inputButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getOutputBluetoothStatus() != ComponentStatus.NONACTIVATED ||
            super.radio.getOutputAuxStatus() != ComponentStatus.NONACTIVATED) {
            outputButtonFailure.setDisable(false);
            if (failureList.contains("outputButton")) {
                outputButtonFailure.setSelected(true);
            } else {
                outputButtonFailure.setSelected(false);
            }
        }
        if (super.radio.getNumberPresets() > 3) {
            preset4ButtonFailure.setDisable(false);
            if (failureList.contains("presetButton4")) {
                preset4ButtonFailure.setSelected(true);
            } else {
                preset4ButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getNumberPresets() > 4) {
            preset5ButtonFailure.setDisable(false);
            if (failureList.contains("presetButton5")) {
                preset5ButtonFailure.setSelected(true);
            } else {
                preset5ButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getNumberPresets() > 5) {
            preset6ButtonFailure.setDisable(false);
            if (failureList.contains("presetButton6")) {
                preset6ButtonFailure.setSelected(true);
            } else {
                preset6ButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getNumberPresets() > 6) {
            preset7ButtonFailure.setDisable(false);
            if (failureList.contains("presetButton7")) {
                preset7ButtonFailure.setSelected(true);
            } else {
                preset7ButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getNumberPresets() > 7) {
            preset8ButtonFailure.setDisable(false);
            if (failureList.contains("presetButton8")) {
                preset8ButtonFailure.setSelected(true);
            } else {
                preset8ButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getNumberPresets() > 8) {
            preset9ButtonFailure.setDisable(false);
            if (failureList.contains("presetButton9")) {
                preset9ButtonFailure.setSelected(true);
            } else {
                preset9ButtonFailure.setSelected(false);
            }
        }

        if (super.radio.getNumberPresets() > 9) {
            preset10ButtonFailure.setDisable(false);
            if (failureList.contains("presetButton10")) {
                preset10ButtonFailure.setSelected(true);
            } else {
                preset10ButtonFailure.setSelected(false);
            }
        }
    }

    public void setComponentsFailure() {

        if (activationFailure.isSelected()) {
            super.radio.getPower().setFailure();
        } else {
            super.radio.getPower().setFunctional();
        }

        if (tunerDABFailure.isSelected()) {
            super.radio.getTuner().setFailure();
        } else {
            super.radio.getTuner().setFunctional();
        }

        if (mainSpeakerFailure.isSelected()) {
            super.radio.getSpeaker().setFailure();
        } else {
            super.radio.getSpeaker().setFunctional();
        }
        if (clockBatteryFailure.isSelected()) {
            super.radio.getClock().setFailure();
        } else {
            super.radio.getClock().setFunctional();
        }

        //----------------------------------------------------//

        if (autotuneDABFailure.isSelected()) {
            super.radio.getAutotune().setFailure();
        } else if (autotuneDABFailure.isDisabled()) {
            super.radio.getAutotune().setNonActivated();
        } else {
            super.radio.getAutotune().setFunctional();
        }

        if (autotuneFMFailure.isSelected()) {
            super.radio.getAutotuneFM().setFailure();
        } else if (autotuneFMFailure.isDisabled()) {
            super.radio.getAutotuneFM().setNonActivated();
        } else {
            super.radio.getAutotuneFM().setFunctional();
        }

        if (secondSpeakerFailure.isSelected()) {
            super.radio.getSecondSpeaker().setFailure();
        } else if (secondSpeakerFailure.isDisabled()) {
            super.radio.getSecondSpeaker().setNonActivated();
        } else {
            super.radio.getSecondSpeaker().setFunctional();
        }

        if (inputBluetoothFailure.isSelected()) {
            super.radio.getBluetooth().setFailure();
        } else if (inputBluetoothFailure.isDisabled()) {
            super.radio.getBluetooth().setNonActivated();
        } else {
            super.radio.getBluetooth().setFunctional();
        }

        if (inputAuxFailure.isSelected()) {
            super.radio.getAux().setFailure();
        } else if (inputAuxFailure.isDisabled()) {
            super.radio.getAux().setNonActivated();
        } else {
            super.radio.getAux().setFunctional();
        }

        if (inputUsbFailure.isSelected()) {
            super.radio.getUsb().setFailure();
        } else if (inputUsbFailure.isDisabled()) {
            super.radio.getUsb().setNonActivated();
        } else {
            super.radio.getUsb().setFunctional();
        }

        if (outputBluetoothFailure.isSelected()) {
            super.radio.setOutputBluetoothStatus(ComponentStatus.FAILURE);
        } else if (outputBluetoothFailure.isDisabled()){
            super.radio.setOutputBluetoothStatus(ComponentStatus.NONACTIVATED);
        } else {
            super.radio.setOutputBluetoothStatus(ComponentStatus.FUNCTIONAL);
        }

        if (outputAuxFailure.isSelected()) {
            super.radio.setOutputAuxStatus(ComponentStatus.FAILURE);
        } else if (outputAuxFailure.isDisabled()){
            super.radio.setOutputAuxStatus(ComponentStatus.NONACTIVATED);
        } else {
            super.radio.setOutputAuxStatus(ComponentStatus.FUNCTIONAL);
        }

        if (alarmFailure.isSelected()) {
            super.radio.getAlarm().setFailure();
        } else if (alarmFailure.isDisabled()) {
            super.radio.getAlarm().setNonActivated();
        } else {
            super.radio.getAlarm().setFunctional();
        }

        if (breakingNewsFailure.isSelected()) {
            super.radio.getBreakingNews().setFailure();
        } else if (breakingNewsFailure.isDisabled()) {
            super.radio.getBreakingNews().setNonActivated();
        } else {
            super.radio.getBreakingNews().setFunctional();
        }

        if (tunerFmFailure.isSelected()) {
            super.radio.getTunerFM().setFailure();
        } else if (tunerFmFailure.isDisabled()) {
            super.radio.getTunerFM().setNonActivated();
        } else {
            super.radio.getTunerFM().setFunctional();
        }

        /* Visual Failures */

        if (displayScreenFailure.isSelected()) {
            failureList.add("display");
        } else {
            failureList.remove("display");
        }

        if (volumeButtonFailure.isSelected()) {
            failureList.add("volumeGroup");
        } else {
            failureList.remove("volumeGroup");
        }

        if (stationButtonFailure.isSelected()) {
            failureList.add("selectionGroup");
        } else {
            failureList.remove("selectionGroup");
        }

        if (confirmButtonFailure.isSelected()) {
            failureList.add("confirmButton");
        } else {
            failureList.remove("confirmButton");
        }

        if (clockButtonFailure.isSelected()) {
            failureList.add("dateTimeButton");
        } else {
            failureList.remove("dateTimeButton");
        }

        if (powerButtonFailure.isSelected()) {
            failureList.add("powerButton");
        } else {
            failureList.remove("powerButton");
        }

        if (preset1ButtonFailure.isSelected()) {
            failureList.add("presetButton1");
        } else {
            failureList.remove("presetButton1");
        }

        if (preset2ButtonFailure.isSelected()) {
            failureList.add("presetButton2");
        } else {
            failureList.remove("presetButton2");
        }

        if (preset3ButtonFailure.isSelected()) {
            failureList.add("presetButton3");
        } else {
            failureList.remove("presetButton3");
        }

        //----------------------------------------------------//

        if (balanceButtonFailure.isSelected()) {
            failureList.add("balanceGroup");
        } else {
            failureList.remove("balanceGroup");
        }

        if (autotuneButtonFailure.isSelected()) {
            failureList.add("autotuneButton");
        } else {
            failureList.remove("autotuneButton");
        }

        if (alarmButtonFailure.isSelected()) {
            failureList.add("alarmButton");
        } else {
            failureList.remove("alarmButton");
        }

        if (inputButtonFailure.isSelected()) {
            failureList.add("inputButton");
        } else {
            failureList.remove("inputButton");
        }

        if (outputButtonFailure.isSelected()) {
            failureList.add("outputButton");
        } else {
            failureList.remove("outputButton");
        }

        if (preset4ButtonFailure.isSelected()) {
            failureList.add("presetButton4");
        } else {
            failureList.remove("presetButton4");
        }

        if (preset5ButtonFailure.isSelected()) {
            failureList.add("presetButton5");
        } else {
            failureList.remove("presetButton5");
        }

        if (preset6ButtonFailure.isSelected()) {
            failureList.add("presetButton6");
        } else {
            failureList.remove("presetButton6");
        }

        if (preset7ButtonFailure.isSelected()) {
            failureList.add("presetButton7");
        } else {
            failureList.remove("presetButton7");
        }

        if (preset8ButtonFailure.isSelected()) {
            failureList.add("presetButton8");
        } else {
            failureList.remove("presetButton8");
        }

        if (preset9ButtonFailure.isSelected()) {
            failureList.add("presetButton9");
        } else {
            failureList.remove("presetButton9");
        }

        if (preset10ButtonFailure.isSelected()) {
            failureList.add("presetButton10");
        } else {
            failureList.remove("presetButton10");
        }
    }

    @Override
    public void handleButtonClick(ActionEvent event) {

    }
}
