package be.ac.umons.controllers;

import be.ac.umons.App;
import be.ac.umons.enumerations.ComponentStatus;
import javafx.scene.Group;

import java.io.IOException;

public abstract class AbstractPlayingController extends AbstractController {

    public Group radioGroup;
    public Group timeDateGroup;

    protected void switchToDateHourMode() {
        this.radioGroup.setVisible(!this.radioGroup.isVisible());
        this.timeDateGroup.setVisible(!this.timeDateGroup.isVisible());
    }
    @Override
    protected void linkFXMLElements() {
        super.linkFXMLElements();
        radioGroup = (Group) App.getFxmlLoader().getNamespace().get("radioGroup");
        timeDateGroup = (Group) App.getFxmlLoader().getNamespace().get("timeDateGroup");
    }

    protected void checkStateDisplay(int idController) {

        if (idController == 2 && super.radio.getBluetooth().isFailing()) {
            displayStation.setText("No Bluetooth Devices detected");
        } else if (idController == 2 ) {
            if (super.radio.getBluetooth().isConnected()) {
                this.displayStation.setText(super.radio.getSong(idController));
            }
        }

        if (idController == 3 && super.radio.getAux().isFailing()) {
            displayStation.setText("No Jack cable detected");
        } else if(idController == 3) {
            this.displayStation.setText(super.radio.getSong(idController));
        }

        if (idController == 4 && super.radio.getUsb().isFailing()) {
            displayStation.setText("No USB Device detected");
        } else if(idController == 4) {
            this.displayStation.setText(super.radio.getSong(idController));
        }
    }

    protected void increaseFrequency(int id){
        if (id == 2 && super.radio.getBluetooth().isConnected() && !super.radio.getBluetooth().isFailing()){
            super.radio.nextSong(id);
        } else if (id == 3 && !super.radio.getAux().isFailing()) {
            super.radio.nextSong(id);
        } else if (id == 4 && !super.radio.getUsb().isFailing()) {
            super.radio.nextSong(id);
        }
        checkStateDisplay(id);
    }

    protected void decreaseFrequency(int id){
        if (id == 2 && super.radio.getBluetooth().isConnected() && !super.radio.getBluetooth().isFailing()){
            super.radio.previousSong(id);
        } else if (id == 3 && !super.radio.getAux().isFailing()) {
            super.radio.previousSong(id);
        } else if (id == 4 && !super.radio.getUsb().isFailing()) {
            super.radio.previousSong(id);
        }
        checkStateDisplay(id);
    }

    protected void changeInput(int idController) {

        if (super.radio.isOutputBluetoothActivated()) {
            super.radio.setOutputBluetoothActivated(false);
        }

        if (idController < 1 && radio.getTunerFM().getStatus() != ComponentStatus.NONACTIVATED) {
            try{
                loadFXML("PlayingFMPanel");
            } catch (IOException ex){
                ex.printStackTrace();
            }
        } else if (idController < 2 && radio.getBluetooth().getStatus() != ComponentStatus.NONACTIVATED) {
            try{
                loadFXML("PlayingBluetoothPanel");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if (idController < 3 && radio.getAux().getStatus() != ComponentStatus.NONACTIVATED) {
            try{
                loadFXML("PlayingAuxPanel");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if (idController < 4 && radio.getUsb().getStatus() != ComponentStatus.NONACTIVATED) {
            try{
                loadFXML("PlayingUsbPanel");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if (idController != 0) {
            try {
                loadFXML("PlayingDABPanel");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
