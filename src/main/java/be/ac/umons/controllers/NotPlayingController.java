package be.ac.umons.controllers;

import be.ac.umons.enumerations.ComponentStatus;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import java.io.IOException;

public class NotPlayingController extends AbstractController {

    @FXML
    private Group timeDateGroup;
    @FXML
    private Button dateTimeButton;

    @FXML
    void initialize() {
        checkComponents();
        super.setClockAnimation();
        super.startClock();
        AdaptDisplayUI();
        setEventTimeSetting(dateTimeButton);
    }

    @Override
    public void checkComponents() {
        super.initializeRadio();
        super.checkComponents();
        super.setEventPresetButton();
        super.setEventAlarmSetting();
    }

    @Override
    public void handleButtonClick(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        switch(btnClicked.getId()) {
            case "dateTimeButton":
                switchToDateHourMode();
                break;
            case "powerButton":
                if (super.radio.getPower().getStatus() != ComponentStatus.FAILURE) {
                    try {
                        super.loadFXML("PlayingDABPanel");
                    }catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
        }
    }

    protected void switchToDateHourMode() {
        super.radio.getClock().setDisplayOn(!super.radio.getClock().isDisplayOn());
        this.AdaptDisplayUI();
    }

    private void AdaptDisplayUI() {
        this.timeDateGroup.setVisible(super.radio.getClock().isDisplayOn());
    }

    private void setEventTimeSetting(Button btn) {
        btn.addEventHandler(MouseEvent.ANY, new EventHandler<>() {
            long startTime;
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
                    startTime = System.currentTimeMillis();
                } else if (mouseEvent.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
                    if (System.currentTimeMillis() - startTime > 3* 1000) {
                        NotPlayingController.super.saveTimeAndDate();
                        try {
                            NotPlayingController.super.loadFXML("TimeSettingPanel");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        switchToDateHourMode();
                    }
                }
            }
        });
    }
}
