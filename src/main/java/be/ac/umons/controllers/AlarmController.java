package be.ac.umons.controllers;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.util.Duration;

import java.io.IOException;

public class AlarmController extends AbstractClockController {

    @FXML
    void initialize() {
        super.checkComponents();
        initializeClock();
    }

    @Override
    public void handleButtonClick(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        switch(btnClicked.getId()) {
            case "powerButton":
                super.radio.toggleAlarm();
                super.turnOffRadio();
                break;
            case "alarmButton":
                super.radio.toggleAlarm();
                try {
                    super.loadFXML("PlayingDABPanel");
                } catch (IOException ex){
                    ex.printStackTrace();
                }
                break;
        }
    }

    private void initializeClock() {
        super.setClockAnimation();
        super.updateTimeAndDateDisplay();
        super.tickingClock.getKeyFrames().add(new KeyFrame(Duration.seconds(1), actionEvent -> {
            super.displayHour.setVisible(!displayHour.isVisible());
            super.displayMinute.setVisible(!displayMinute.isVisible());
        }));

        super.tickingClock.play();
    }
}
