package be.ac.umons.controllers;

import be.ac.umons.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;

public class PlayingBluetoothController extends AbstractPlayingController {

    private int idController = 2;

    @FXML
    void initialize() {
        super.initializeRadio();
        super.checkComponents();
        super.setEventPresetButton();
        linkBluetoothUI();
        super.setClockAnimation();
        super.startClock();
        super.setEventAlarmSetting();
        super.displayMessage("Bluetooth", 2);
    }

    public void handleButtonClick(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        switch(btnClicked.getId()) {
            case "dateTimeButton":
                super.switchToDateHourMode();
                break;
            case "powerButton":
                super.turnOffRadio();
                break;
            case "volumeIncreaseButton":
                super.displayMessage("Volume " +  super.radio.increaseVolume(), 1);
                break;
            case "volumeDecreaseButton":
                super.displayMessage("Volume " +  super.radio.decreaseVolume(), 1);
                break;
            case "balanceIncreaseButton":
                super.displayMessage("Balance " +  super.radio.increaseBalance(), 1);
                break;
            case "balanceDecreaseButton":
                super.displayMessage("Balance " +  super.radio.decreaseBalance(), 1);
                break;
            case "frequencyIncreaseButton":
                super.increaseFrequency(idController);
                break;
            case "frequencyDecreaseButton":
                super.decreaseFrequency(idController);
                break;
            case "inputButton":
                 super.changeInput(idController);
                break;
        }
    }

    private void linkBluetoothUI() {
        if (!super.radio.getBluetooth().isFailing()){
            this.displayStation.setText("Pairing...");
        } else {
            super.checkStateDisplay(idController);
        }
        MenuItem bluetoothPairing = (MenuItem) App.getFxmlLoader().getNamespace().get("bluetoothPairing");
        bluetoothPairing.setVisible(true);
    }

    public void pairDevice() {
        if(!super.radio.getBluetooth().isFailing()) {
            super.radio.getBluetooth().setConnected(true);
            super.checkStateDisplay(idController);
            super.displayMessage("Appareil Bluetooth connecté", 2);
        }
    }
}
