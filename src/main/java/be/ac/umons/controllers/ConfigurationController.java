package be.ac.umons.controllers;

import be.ac.umons.enumerations.ComponentStatus;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.ToggleButton;

import java.io.IOException;

public class ConfigurationController extends AbstractController {

    @FXML
    private ToggleButton autotuneActivation;
    @FXML
    private ToggleButton secondSpeakerActivation;
    @FXML
    private ToggleButton inputBluetooth;
    @FXML
    private ToggleButton inputAux;
    @FXML
    private ToggleButton inputUsb;
    @FXML
    private ToggleButton outputBluetooth;
    @FXML
    private ToggleButton outputAux;
    @FXML
    private ToggleButton automaticClock;
    @FXML
    private ToggleButton alarm;
    @FXML
    private ToggleButton breakingNews;
    @FXML
    private ToggleButton radioFm;
    @FXML
    private ChoiceBox screenSize;
    @FXML
    private Spinner numberPresets;

    @FXML
    public void initialize() {
        checkComponents();
    }

    public void switchToFailurePanel() throws IOException {
        toggleComponents();
        super.loadFXML("FailurePanel");
    }

    private void toggleComponents() {

        int presetsButtonNumber = (Integer) numberPresets.getValue();
        super.radio.setNumberPresets(presetsButtonNumber);

        //------------------------------------------------------------------------------------------------------------//

        if (autotuneActivation.isSelected() && super.radio.getAutotune().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getAutotune().setFunctional();
            if (radioFm.isSelected()) {
                super.radio.getAutotuneFM().setFunctional();
            }
        } else if (!autotuneActivation.isSelected() && super.radio.getAutotune().getStatus() != ComponentStatus.FAILURE) {
            super.radio.getAutotune().setNonActivated();
            super.radio.getAutotuneFM().setNonActivated();
        }

        if (secondSpeakerActivation.isSelected() && super.radio.getSecondSpeaker().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getSecondSpeaker().setFunctional();
        } else if (!secondSpeakerActivation.isSelected()) {
            super.radio.getSecondSpeaker().setNonActivated();
        }

        if (inputBluetooth.isSelected() && super.radio.getBluetooth().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getBluetooth().setFunctional();
        } else if (!inputBluetooth.isSelected()) {
            super.radio.getBluetooth().setNonActivated();
        }

        if (inputAux.isSelected() && super.radio.getAux().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getAux().setFunctional();
        } else if (!inputAux.isSelected()) {
            super.radio.getAux().setNonActivated();
        }

        if (inputUsb.isSelected() && super.radio.getUsb().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getUsb().setFunctional();
        } else if (!inputUsb.isSelected()) {
            super.radio.getUsb().setNonActivated();
        }

        if (outputBluetooth.isSelected() && super.radio.getOutputBluetoothStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.setOutputBluetoothStatus(ComponentStatus.FUNCTIONAL);
        } else if (!outputBluetooth.isSelected()) {
            super.radio.setOutputBluetoothStatus(ComponentStatus.NONACTIVATED);
        }

        if (outputAux.isSelected() && super.radio.getOutputAuxStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.setOutputAuxStatus(ComponentStatus.FUNCTIONAL);
        } else if (!outputAux.isSelected()) {
            super.radio.setOutputAuxStatus(ComponentStatus.NONACTIVATED);
        }

        if (automaticClock.isSelected()) {
            super.radio.getClock().setAutomatic(true);
        } else if (!automaticClock.isSelected()) {
            super.radio.getClock().setAutomatic(false);
        }

        if (alarm.isSelected() && super.radio.getAlarm().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getAlarm().setFunctional();
        } else if (!alarm.isSelected()) {
            super.radio.getAlarm().setNonActivated();
        }

        if (breakingNews.isSelected() && super.radio.getBreakingNews().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getBreakingNews().setFunctional();
        } else if (!breakingNews.isSelected()) {
            super.radio.getBreakingNews().setNonActivated();
        }

        if (radioFm.isSelected() && super.radio.getTunerFM().getStatus() == ComponentStatus.NONACTIVATED) {
            super.radio.getTunerFM().setFunctional();
        } else if (!radioFm.isSelected()) {
            super.radio.getTunerFM().setNonActivated();
        }
    }

    @Override
    protected void checkComponents() {
        super.initializeRadio();

        numberPresets.getValueFactory().setValue(radio.getNumberPresets());

        if (super.radio.getAutotune().getStatus() != ComponentStatus.NONACTIVATED) {
            autotuneActivation.setSelected(true);
        }

        if (super.radio.getSecondSpeaker().getStatus() != ComponentStatus.NONACTIVATED){
            secondSpeakerActivation.setSelected(true);
        }

        if (super.radio.getBluetooth().getStatus() != ComponentStatus.NONACTIVATED) {
            inputBluetooth.setSelected(true);
        }

        if (super.radio.getAux().getStatus() != ComponentStatus.NONACTIVATED) {
            inputAux.setSelected(true);
        }

        if (super.radio.getUsb().getStatus() != ComponentStatus.NONACTIVATED) {
            inputUsb.setSelected(true);
        }

        if (super.radio.getOutputBluetoothStatus() != ComponentStatus.NONACTIVATED) {
            outputBluetooth.setSelected(true);
        }

        if (super.radio.getOutputAuxStatus() != ComponentStatus.NONACTIVATED) {
            outputAux.setSelected(true);
        }

        if (super.radio.getClock().isAutomatic()) {
            automaticClock.setSelected(true);
        }

        if (super.radio.getAlarm().getStatus() != ComponentStatus.NONACTIVATED) {
            alarm.setSelected(true);
        }

        if (super.radio.getBreakingNews().getStatus() != ComponentStatus.NONACTIVATED) {
            breakingNews.setSelected(true);
        }

        if (super.radio.getTunerFM().getStatus() != ComponentStatus.NONACTIVATED) {
            radioFm.setSelected(true);
        }

    }

    public void switchToSimulationPanel() throws IOException {
        toggleComponents();
        super.loadFXML("NotPlayingPanel");
    }

    @Override
    public void handleButtonClick(ActionEvent event) {

    }
}
