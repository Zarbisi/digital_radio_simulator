package be.ac.umons.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class PlayingUsbController extends AbstractPlayingController {

    private int idController = 4;

    @FXML
    void initialize() {
        super.initializeRadio();
        super.checkComponents();
        super.setEventPresetButton();
        super.setClockAnimation();
        super.startClock();
        super.checkStateDisplay(idController);
        super.setEventAlarmSetting();
        super.displayMessage("USB", 2);
    }

    public void handleButtonClick(ActionEvent event) {
        Button btnClicked = (Button) event.getSource();
        switch(btnClicked.getId()) {
            case "dateTimeButton":
                super.switchToDateHourMode();
                break;
            case "powerButton":
                super.turnOffRadio();
                break;
            case "volumeIncreaseButton":
                super.displayMessage("Volume " +  super.radio.increaseVolume(), 1);
                break;
            case "volumeDecreaseButton":
                super.displayMessage("Volume " +  super.radio.decreaseVolume(), 1);
                break;
            case "balanceIncreaseButton":
                super.displayMessage("Balance " +  super.radio.increaseBalance(), 1);
                break;
            case "balanceDecreaseButton":
                super.displayMessage("Balance " +  super.radio.decreaseBalance(), 1);
                break;
            case "frequencyIncreaseButton":
                super.increaseFrequency(idController);
                break;
            case "frequencyDecreaseButton":
                super.decreaseFrequency(idController);
                break;
            case "inputButton":
                super.changeInput(idController);
                break;
            case "autotuneButton":
                break;
            case "nextButton":
                break;
            case "previousButton":
                break;
            case "alarmButton":
                break;
            case "alarmSettingButton":
                break;
        }
    }
}
