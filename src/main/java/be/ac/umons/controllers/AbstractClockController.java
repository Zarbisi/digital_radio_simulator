package be.ac.umons.controllers;

import be.ac.umons.App;
import javafx.animation.PauseTransition;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.text.DecimalFormat;

public abstract class AbstractClockController extends AbstractController {

    protected DecimalFormat decimalFormat = new DecimalFormat("00");

    public Text displayMinute;
    public Text displaySecond;
    public Text displayDateDay;
    public Text displayDateMonth;
    public Text displayDateYear;

    @Override
    protected void checkComponents() {
        super.initializeRadio();
        super.checkComponents();
        displayButtonPreset();
    }

    private void displayButtonPreset() {
        for (int i = 0; i < super.radio.getNumberPresets(); i ++) {
            String name = "presetButton" +(i+1);
            Button btn = (Button) App.getFxmlLoader().getNamespace().get(name);
            btn.setVisible(true);
        }
    }

    @Override
    protected void updateTimeAndDateDisplay() {
        super.linkFXMLElements();
        super.displayHour.setText(decimalFormat.format(super.hours.intValue()));
        displayMinute.setText(":"+ decimalFormat.format(super.minutes.intValue()) + ":");
        displaySecond.setText(decimalFormat.format(super.seconds.intValue()));
        displayDateDay.setText(decimalFormat.format(super.day.intValue()));
        displayDateMonth.setText("-" + decimalFormat.format(super.month.intValue()) + "-");
        displayDateYear.setText(decimalFormat.format(super.year.intValue()));
    }

    protected void displayTime() {
        displayHour.setVisible(true);
        displayMinute.setVisible(true);
        displaySecond.setVisible(true);
        displayDateDay.setVisible(false);
        displayDateMonth.setVisible(false);
        displayDateYear.setVisible(false);
    }

    protected void displayDate() {
        displayHour.setVisible(false);
        displayMinute.setVisible(false);
        displaySecond.setVisible(false);
        displayDateDay.setVisible(true);
        displayDateMonth.setVisible(true);
        displayDateYear.setVisible(true);
    }

    @Override
    protected void displayMessage(String message , int duration) {
        super.tickingClock.pause();
        this.displayHour.setVisible(false);
        this.displayMinute.setVisible(false);
        this.displayMessage.setText(message);
        this.displayMessage.setVisible(true);

        PauseTransition wait = new PauseTransition(Duration.seconds(duration));
        wait.setOnFinished((e) -> {
            this.displayMessage.setVisible(false);
            this.displayHour.setVisible(true);
            this.displayMinute.setVisible(true);
            super.tickingClock.play();
        });
        wait.play();
    }
}
