package be.ac.umons.models;

public class RadioStation {
    private String name;
    private Double frequency;

    public RadioStation(String name , Double frequency) {
        this.name = name;
        this.frequency = frequency;
    }

    public String getName() {
        return name;
    }
    public Double getFrequency() {
        return frequency;
    }
}
