package be.ac.umons.models;

public class RadioDABStation {
    private String channel;
    private String station;

    public RadioDABStation(String channel, String station) {
        this.channel = channel;
        this.station = station;
    }

    public String getChannel() {
        return channel;
    }

    public String getStationName() {
        return station;
    }
}
