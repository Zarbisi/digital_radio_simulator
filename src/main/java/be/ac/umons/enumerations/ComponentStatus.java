package be.ac.umons.enumerations;

public enum ComponentStatus {
    FUNCTIONAL,
    FAILURE,
    NONACTIVATED
}
