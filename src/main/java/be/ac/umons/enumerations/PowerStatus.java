package be.ac.umons.enumerations;

public enum PowerStatus {
    ON,
    OFF
}
