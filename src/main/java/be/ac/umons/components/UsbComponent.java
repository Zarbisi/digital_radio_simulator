package be.ac.umons.components;

public class UsbComponent extends InputComponent {

    public UsbComponent() {
        populateSongList();
        super.actualSong = super.songs.get(super.indexSong);
    }

    @Override
    protected void populateSongList() {
        super.songs.add("The Weeknd - Scared To Live");
        super.songs.add("Magoyond - Le Pudding à l'Arsenic");
        super.songs.add("Aaron Smith, Krono - Dancin - Krono Remix");
        super.songs.add("The HU, Papa Roach - Wolf Totem");
        super.songs.add("KONGOS - Come with Me Now");
        super.songs.add("Owl City - Not All Heroes Wear Cape");
    }
}
