package be.ac.umons.components;

import be.ac.umons.enumerations.ComponentStatus;
import be.ac.umons.enumerations.PowerStatus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Radio {
    private static Radio instance;
    private PowerComponent power;
    private SpeakerComponent speaker;
    private AdditionalSpeakerComponent secondSpeaker;
    private AlarmComponent alarm;
    private AutotuneComponent autotune;
    private AutotuneFMComponent autotuneFM;
    private AuxilliaireComponent aux;
    private BluetoothComponent bluetooth;
    private BreakingNewsComponent breakingNews;
    private ClockComponent clock;
    private TunerComponent tuner;
    private TunerFMComponent tunerFM;
    private UsbComponent usb;

    private ComponentStatus outputAuxStatus = ComponentStatus.NONACTIVATED;
    private ComponentStatus outputBluetoothStatus = ComponentStatus.NONACTIVATED;
    private boolean outputBluetoothActivated = false;
    private boolean outputAuxActivated = false;
    private int outputState = 0;

    private int numberPresets = 3;

    private List<Integer> presetsFM = new ArrayList<>();
    private List<Integer> presetsDAB = new ArrayList<>();

    private HashSet<String> failureIdsUI = new HashSet<>();

    private Radio() {

        alarm = new AlarmComponent();
        autotune = new AutotuneComponent(this);
        autotuneFM = new AutotuneFMComponent(this);
        aux = new AuxilliaireComponent();
        bluetooth = new BluetoothComponent();
        breakingNews = new BreakingNewsComponent();
        clock = new ClockComponent();
        power = new PowerComponent();
        speaker = new SpeakerComponent();
        secondSpeaker = new AdditionalSpeakerComponent();
        tuner = new TunerComponent();
        tunerFM = new TunerFMComponent();
        usb = new UsbComponent();
    }

    public static Radio getInstance() {
        if (instance == null) {
            instance = new Radio();
        }
        return instance;
    }

    public PowerStatus switchOnOff() {
        return power.changeStatus();
    }

    public PowerStatus getPowerStatus() {
        return  power.getPowerStatus();
    }

    public int increaseVolume() {
        speaker.increaseVolume();
        return speaker.getVolume();
    }

    public int decreaseVolume() {
        speaker.decreaseVolume();
        return speaker.getVolume();
    }

    public int increaseBalance() {
        secondSpeaker.increaseBalance();
        return secondSpeaker.getBalance();
    }

    public int decreaseBalance() {
        secondSpeaker.decreaseBalance();
        return secondSpeaker.getBalance();
    }

    public void nextChannel() { tuner.nextChannel(); }

    public void previousChannel() { tuner.previousChannel(); }

    public void increaseFrequency() {
        tunerFM.increaseFrequency();
    }

    public void decreaseFrequency() {
        tunerFM.decreaseFrequency();
    }

    public String getActualFrequency() {
        return tunerFM.getFrequency();
    }

    public String getActualRadio(int id) {
        if (id == 0) {
            return tuner.getActualRadio();
        } else if (id == 1) {
            return tunerFM.getActualRadio();
        }
        return null;
    }

    public String getActualChannel() {
        return tuner.getActualChannel();
    }

    public int getActualIndex() {
        return tuner.getActualIndex();
    }

    public void loadPreset(int id, int idController) {
        if (idController == 0) {
            tuner.setChannelIndex(presetsDAB.get(id));
        } else {
            tunerFM.setFrequencyIndex(presetsFM.get(id));
        }
    }

    public List<Integer> getPresets(int idController) {
        if (idController == 0) {
            return presetsDAB;
        } else {
            return presetsFM;
        }
    }

    public String broadcastNews() {
        return breakingNews.getBroadcastNews();
    }

    public void saveTime(int hours, int minutes, int seconds) {
        clock.setHours(hours);
        clock.setMinutes(minutes);
        clock.setSeconds(seconds);
    }

    public void saveDate(int day, int month, int year) {
        clock.setDay(day);
        clock.setMonth(month);
        clock.setYear(year);
    }

    public PowerComponent getPower() {
        return power;
    }

    public TunerComponent getTuner() {
        return tuner;
    }

    public TunerFMComponent getTunerFM() {
        return tunerFM;
    }

    public BluetoothComponent getBluetooth() {
        return bluetooth;
    }

    public AuxilliaireComponent getAux() {
        return aux;
    }

    public UsbComponent getUsb() {
        return usb;
    }

    public ClockComponent getClock() {
        return clock;
    }

    public AdditionalSpeakerComponent getSecondSpeaker() {
        return secondSpeaker;
    }

    public SpeakerComponent getSpeaker() {
        return speaker;
    }

    public BreakingNewsComponent getBreakingNews() {
        return breakingNews;
    }

    public int getNumberPresets() {
        return numberPresets;
    }

    public void setNumberPresets(int numberPresets) {
        this.numberPresets = numberPresets;
    }

    public ComponentStatus getOutputAuxStatus() {
        return outputAuxStatus;
    }

    public void setOutputAuxStatus(ComponentStatus outputAuxStatus) {
        this.outputAuxStatus = outputAuxStatus;
    }

    public ComponentStatus getOutputBluetoothStatus() {
        return outputBluetoothStatus;
    }

    public void setOutputBluetoothStatus(ComponentStatus outputBluetoothStatus) {
        this.outputBluetoothStatus = outputBluetoothStatus;
    }

    public boolean isOutputBluetoothActivated() {
        return outputBluetoothActivated;
    }

    public void setOutputBluetoothActivated(boolean outputBluetoothActivated) {
        this.outputBluetoothActivated = outputBluetoothActivated;
    }

    public boolean isOutputAuxActivated() {
        return outputAuxActivated;
    }

    public void setOutputAuxActivated(boolean outputAuxActivated) {
        this.outputAuxActivated = outputAuxActivated;
    }

    public AutotuneComponent getAutotune() {
        return autotune;
    }

    public AutotuneFMComponent getAutotuneFM() {
        return autotuneFM;
    }

    /* Alarm Related Methods */
    public AlarmComponent getAlarm() {
        return alarm;
    }

    public boolean toggleAlarm() {
        return alarm.toggleAlarm();
    }

    public boolean isAlarmActivated() {
        return alarm.isActivated();
    }

    public int getAlarmHour() {
        return alarm.getHour();
    }

    public int getAlarmMinute() {
        return alarm.getMinute();
    }

    public void setAlarmHour(int hour) {
        alarm.setHour(hour);
    }

    public void setAlarmMinute(int minute) {
        alarm.setMinute(minute);
    }

    public boolean checkAlarmSound() {
        return alarm.isAlarmSound();
    }

    public void setAlarmSound(boolean alarmSound) {
        alarm.setAlarmSound(alarmSound);
    }
    /* */

    /* Song Related Methods */
    public String getSong(int id) {
        String song = null;
        switch(id) {
            case 2:
                song = bluetooth.getActualSong();
                break;
            case 3:
                song = aux.getActualSong();
                break;
            case 4:
                song = usb.getActualSong();
                break;
        }
        return song;
    }

    public void nextSong(int id) {
        switch(id) {
            case 2:
                bluetooth.next();
                break;
            case 3:
                aux.next();
                break;
            case 4:
                usb.next();
                break;
        }
    }

    public void previousSong(int id) {
        switch(id) {
            case 2:
                bluetooth.previous();
                break;
            case 3:
                aux.previous();
                break;
            case 4:
                usb.previous();
                break;
        }
    }
    /* */

    public HashSet<String> getFailureIdsUI() {
        return failureIdsUI;
    }

    public void setFailureIdsUI(HashSet<String> failureIdsUI) {
        this.failureIdsUI = failureIdsUI;
    }

    public int getOutputState() {
        return outputState;
    }

    public void setOutputState(int outputState) {
        this.outputState = outputState;
    }
}
