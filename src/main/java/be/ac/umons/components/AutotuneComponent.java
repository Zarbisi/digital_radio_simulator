package be.ac.umons.components;

import be.ac.umons.models.RadioDABStation;

import java.util.ArrayList;
import java.util.List;

public class AutotuneComponent extends RadioComponent {
    Radio radio;
    private List<RadioDABStation> autotuneDABList = new ArrayList<>();

    public AutotuneComponent(Radio radio) {
        this.radio = radio;
    }

    public List<RadioDABStation> setAutotuneList() {
        autotuneDABList = new ArrayList<>();
        int baseIndex = radio.getActualIndex();
        int actualIndex = radio.getActualIndex();
        for (int i=0; i < radio.getTuner().getChannels().size(); i++) {
            autotuneDABList.add(radio.getTuner().getChannels().get(actualIndex));

            if (actualIndex < radio.getTuner().getChannels().size() - 1) {
                actualIndex++;
            } else if (actualIndex == baseIndex - 1) {
                break;
            } else {
                actualIndex = 0;
            }
        }
        return autotuneDABList;
    }
}
