package be.ac.umons.components;

import be.ac.umons.models.RadioDABStation;
import be.ac.umons.models.RadioStation;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TunerFMComponent extends RadioComponent {
    private static TunerFMComponent instance;

    private double frequency;
    private DecimalFormatSymbols frequencyFormat;
    private DecimalFormat decimalFormat;
    private List<RadioStation> stations = new ArrayList<>();
    private int frequencyIndex = 0;

    public TunerFMComponent() {
       frequencyFormat = new DecimalFormatSymbols(Locale.getDefault());
       frequencyFormat.setDecimalSeparator('.');
       decimalFormat= new DecimalFormat("##.00", frequencyFormat);
       AddStationsList();
       frequency = stations.get(frequencyIndex).getFrequency();
    }

    private void AddStationsList() {
        stations.add(new RadioStation("Ramdam Musique", 87.70));
        stations.add(new RadioStation("Nostalgie", 87.80));
        stations.add(new RadioStation("France Bleu Nord", 88.10));
        stations.add(new RadioStation("Sud Radio", 88.20));
        stations.add(new RadioStation("NRJ", 88.70));
        stations.add(new RadioStation("VRT Klara", 89.50));
        stations.add(new RadioStation("Radyo Pasa", 89.70));
        stations.add(new RadioStation("France Culture", 90.20));
        stations.add(new RadioStation("Sud Radio", 90.30));
        stations.add(new RadioStation("Fun Radio",  90.60));
        stations.add(new RadioStation("Classic 21", 90.80));
        stations.add(new RadioStation("VRT Radio 1", 90.80));
        stations.add(new RadioStation("NRJ ", 91.90));
        stations.add(new RadioStation("VivaCite Charleroi", 92.30));
        stations.add(new RadioStation("Musiq'3", 92.80));
        stations.add(new RadioStation("La Premiere", 93.40));
        stations.add(new RadioStation("VRT Radio 2", 93.70));
        stations.add(new RadioStation("Buzz radio", 94.30));
        stations.add(new RadioStation("La Premiere", 94.80));
        stations.add(new RadioStation("France Musique", 95.20));
        stations.add(new RadioStation("RFM", 95.90));
        stations.add(new RadioStation("La Premiere", 96.10));
        stations.add(new RadioStation("Skyrock", 96.30));
        stations.add(new RadioStation("Pure", 96.60));
        stations.add(new RadioStation("Musiq'3", 97.10));
        stations.add(new RadioStation("Europe 1", 97.70));
        stations.add(new RadioStation("buzz radio", 97.80));
        stations.add(new RadioStation("VivaCite Namur", 98.30));
        stations.add(new RadioStation("Classic 21", 99.10));
        stations.add(new RadioStation("Nostalgie", 100.00));
        stations.add(new RadioStation("France Inter", 100.30));
        stations.add(new RadioStation("VRT Studio Brussel", 100.60));
        stations.add(new RadioStation("Radio Contact", 100.80));
        stations.add(new RadioStation("Pure", 101.10));
        stations.add(new RadioStation("DH Radio", 101.40));
        stations.add(new RadioStation("Radio Contact", 102.20));
        stations.add(new RadioStation("La Premiere", 102.70));
        stations.add(new RadioStation("Radio Canal FM", 102.80));
        stations.add(new RadioStation("Qmusic", 103.10));
        stations.add(new RadioStation("Fun Radio", 103.50));
        stations.add(new RadioStation("Bel RTL", 104.00));
        stations.add(new RadioStation("AFN Benelux", 104.20));
        stations.add(new RadioStation("NRJ", 104.80));
        stations.add(new RadioStation("Must FM", 105.10));
        stations.add(new RadioStation("Ramdam Musique", 105.60));
        stations.add(new RadioStation("Radio J600", 106.10));
        stations.add(new RadioStation("France Info", 106.20));
        stations.add(new RadioStation("Charleking Radio", 106.50));
        stations.add(new RadioStation("Radio Columbia", 106.90));
        stations.add(new RadioStation("Nostalgie", 107.00));
        stations.add(new RadioStation("Fun Radio", 107.20));
        stations.add(new RadioStation("Radio Contact", 107.80));
        stations.add(new RadioStation("Radio Bonheur", 107.90));
    }

    public void increaseFrequency(){
        if (frequencyIndex == (stations.size() -1)) {
            frequencyIndex = 0;
        } else {
            frequencyIndex++;
        }
        frequency = roundFrequencyValue(stations.get(frequencyIndex).getFrequency());
    }

    public void decreaseFrequency(){
        if (frequencyIndex == 0) {
            frequencyIndex = (stations.size() - 1);
        } else {
            frequencyIndex--;
        }
        frequency = roundFrequencyValue(stations.get(frequencyIndex).getFrequency());
    }

    private double roundFrequencyValue(double valueToRound) {
        return (double) Math.round(valueToRound * 100) / 100;
    }

    public String getFrequency() {
        return decimalFormat.format(frequency);
    }

    public int getCorrespondingIndex(RadioStation object) {
        return stations.indexOf(object);
    }

    public String getActualRadio() {
        return stations.get(frequencyIndex).getName();
    }

    public int getFrequencyIndex() {
        return frequencyIndex;
    }

    public void setFrequencyIndex(int frequencyIndex) {
        this.frequencyIndex = frequencyIndex;
        this.frequency = stations.get(frequencyIndex).getFrequency();
    }

    public List<RadioStation> getStations() {
        return stations;
    }
}
