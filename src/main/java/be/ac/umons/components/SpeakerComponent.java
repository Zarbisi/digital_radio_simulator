package be.ac.umons.components;

public class SpeakerComponent extends RadioComponent {

    private int volume;
    private final int MAX_VOLUME = 20;
    private final int MIN_VOLUME = 0;

    public SpeakerComponent() {
        this.volume = 10;
    }

    public void increaseVolume() {
        if (volume < MAX_VOLUME) {
            volume++;
        }
    }

    public void decreaseVolume() {
        if (volume > MIN_VOLUME) {
            volume--;
        }
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
