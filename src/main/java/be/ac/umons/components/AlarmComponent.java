package be.ac.umons.components;

public class AlarmComponent extends RadioComponent {

    private boolean isActivated = false;
    private boolean alarmSound = true;
    private int hour = 0;
    private int minute = 0;

    public AlarmComponent() {}


    public boolean toggleAlarm() {
        isActivated = !isActivated;
        return isActivated;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        if (hour > 23) {
            this.hour = 23;
        } else if (hour < 0) {
            this.hour = 0;
        }else {
            this.hour = hour;
        }
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        if (minute > 59) {
            this.minute = 59;
        } else if (minute < 0) {
            this.minute = 0;
        }else {
            this.minute = minute;
        }
    }

    public boolean isActivated() {
        return isActivated;
    }

    public boolean isAlarmSound() {
        return alarmSound;
    }

    public void setAlarmSound(boolean alarmSound) {
        this.alarmSound = alarmSound;
    }
}
