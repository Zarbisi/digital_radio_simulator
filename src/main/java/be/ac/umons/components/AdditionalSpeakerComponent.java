package be.ac.umons.components;

public class AdditionalSpeakerComponent extends RadioComponent {

    private int balance;
    private final int MAX_BALANCE = 10;
    private final int MIN_BALANCE = -10;

    public AdditionalSpeakerComponent() {}

    public void increaseBalance() {
        if (balance < MAX_BALANCE) {
            balance++;
        }
    }

    public void decreaseBalance() {
        if (balance > MIN_BALANCE) {
            balance--;
        }
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
