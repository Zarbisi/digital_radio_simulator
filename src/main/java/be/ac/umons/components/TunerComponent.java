package be.ac.umons.components;

import be.ac.umons.models.RadioDABStation;

import java.util.ArrayList;
import java.util.List;

public class TunerComponent extends RadioComponent {
    private List<RadioDABStation> channels = new ArrayList<>();
    private String actualChannel;
    private int channelIndex = 0;

    public TunerComponent() {
        this.addDABRadioToMemory();
        actualChannel = channels.get(0).getChannel();
    }

    public void nextChannel() {
        if (checkBorders(true)) {
            channelIndex++;
        } else {
            channelIndex = 0;
        }
        actualChannel = channels.get(channelIndex).getChannel();
    }

    public void previousChannel() {
        if (checkBorders(false)) {
            channelIndex--;
        } else {
            channelIndex = (channels.size() - 1);
        }
        actualChannel = channels.get(channelIndex).getChannel();
    }

    private boolean checkBorders( boolean forward) {
        if (channelIndex == 0 && !forward) {
            return false;
        } else if (channelIndex == (channels.size() - 1) && forward) {
            return false;
        } else {
            return true;
        }
    }

    public String getActualChannel() {
        return actualChannel;
    }

    public String getActualRadio() {
        return channels.get(channelIndex).getStationName();
    }

    public int getActualIndex() {
        return channelIndex;
    }

    public int getCorrespondingIndex(RadioDABStation object) {
        return channels.indexOf(object);
    }

    public void setChannelIndex(int id) {
        this.channelIndex = id;
        actualChannel = channels.get(channelIndex).getChannel();
    }

    private void addDABRadioToMemory() {
        channels.add(new RadioDABStation("5B", "La Première"));
        channels.add(new RadioDABStation("5B","Classic 21"));
        channels.add(new RadioDABStation("5B", "VivaCité Luxembourg"));
        channels.add(new RadioDABStation("5B", "VivaCité Namur"));
        channels.add(new RadioDABStation("5B", "Bel RTL"));
        channels.add(new RadioDABStation("5B", "Radio Contact"));
        channels.add(new RadioDABStation("5B", "Nostalgie"));
        channels.add(new RadioDABStation("5B", "NRJ"));

        channels.add(new RadioDABStation("5C", "La Première"));
        channels.add(new RadioDABStation("5C", "Classic 21"));
        channels.add(new RadioDABStation("5C", "VivaCité Charleroi"));
        channels.add(new RadioDABStation("5C", "VivaCité Hainaut"));
        channels.add(new RadioDABStation("5C", "Bel RTL"));
        channels.add(new RadioDABStation("5C", "Radio Contact"));
        channels.add(new RadioDABStation("5C", "Nostalgie"));
        channels.add(new RadioDABStation("5C", "NRJ"));

        channels.add(new RadioDABStation("5D", "Joe 90's"));
        channels.add(new RadioDABStation("5D","Joe Easy"));
        channels.add(new RadioDABStation("5D",  "NRJ"));
        channels.add(new RadioDABStation("5D", "Q-Foute Radio"));
        channels.add(new RadioDABStation("5D", "RTBF Mix"));
        channels.add(new RadioDABStation("5D", "Stadsradio Vlaanderen"));
        channels.add(new RadioDABStation("5D", "VRBO Evergreen"));
        channels.add(new RadioDABStation("5D", "Willy"));

        channels.add(new RadioDABStation("6A", "Musiq3"));
        channels.add(new RadioDABStation("6A", "Pure"));
        channels.add(new RadioDABStation("6A", "TARMAC"));
        channels.add(new RadioDABStation("6A", "DH Radio"));
        channels.add(new RadioDABStation("6A", "Fun Radio"));
        channels.add(new RadioDABStation("6A", "Sud Radio"));

        channels.add(new RadioDABStation("6C", "BRF Radio 1"));
        channels.add(new RadioDABStation("6C", "BRF Radio 2"));
        channels.add(new RadioDABStation("6C", "Musiq3"));
        channels.add(new RadioDABStation("6C", "Pure"));
        channels.add(new RadioDABStation("6C", "TARMAC"));
        channels.add(new RadioDABStation("6C", "DH Radio"));
        channels.add(new RadioDABStation("6C", "Fun Radio"));
        channels.add(new RadioDABStation("6C", "Must FM"));

        channels.add(new RadioDABStation("6D", "BRF Radio 1"));
        channels.add(new RadioDABStation("6D", "BRF Radio 2"));
        channels.add(new RadioDABStation("6D", "Musiq3"));
        channels.add(new RadioDABStation("6D", "Pure"));
        channels.add(new RadioDABStation("6D", "TARMAC"));
        channels.add(new RadioDABStation("6D", "Antipode"));
        channels.add(new RadioDABStation("6D", "Chérie FM"));
        channels.add(new RadioDABStation("6D","DH Radio"));
        channels.add(new RadioDABStation("6D", "Fun Radio"));

        channels.add(new RadioDABStation("11A", "BBC World Service"));
        channels.add(new RadioDABStation("11A", "Family Radio"));
        channels.add(new RadioDABStation("11A", "Joe"));
        channels.add(new RadioDABStation("11A", "Joe 60's & 70's"));
        channels.add(new RadioDABStation("11A", "Joe 80's"));
        channels.add(new RadioDABStation("11A", "Radio Maria"));
        channels.add(new RadioDABStation("11A", "Nostalgie"));
        channels.add(new RadioDABStation("11A", "Qmusic"));
        channels.add(new RadioDABStation("11A", "Q-Maximum Hits"));
        channels.add(new RadioDABStation("11A", "ROXX Radio"));
        channels.add(new RadioDABStation("11A", "TOPradio"));
        channels.add(new RadioDABStation("11A", "VBRO Radio"));

        channels.add(new RadioDABStation("11D", "La Première (Bxl)"));
        channels.add(new RadioDABStation("11D", "Classic 21"));
        channels.add(new RadioDABStation("11D", "VivaCité Bruxelles"));
        channels.add(new RadioDABStation("11D", "VivaCité Brabant Wallon"));
        channels.add(new RadioDABStation("11D", "Bel RTL"));
        channels.add(new RadioDABStation("11D", "Radio Contact"));
        channels.add(new RadioDABStation("11D", "Nostalgie"));
        channels.add(new RadioDABStation("11D", "NRJ"));

        channels.add(new RadioDABStation("12A", "VRT Radio 1"));
        channels.add(new RadioDABStation("12A", "VRT Radio 2 (Ant)"));
        channels.add(new RadioDABStation("12A", "VRT Radio 2 (Lim)"));
        channels.add(new RadioDABStation("12A", "VRT Radio 2 (O-Vl)"));
        channels.add(new RadioDABStation("12A", "VRT Radio 2 (Vl-Br)"));
        channels.add(new RadioDABStation("12A", "VRT Radio 2 (W-Vl)"));
        channels.add(new RadioDABStation("12A", "VRT Klara"));
        channels.add(new RadioDABStation("12A", "VRT Klara Continuo"));
        channels.add(new RadioDABStation("12A", "VRT Studio Brussel"));
        channels.add(new RadioDABStation("12A", "VRT MNM"));
        channels.add(new RadioDABStation("12A", "VRT MNM Hits"));
        channels.add(new RadioDABStation("12A", "VRT NWS"));
    }

    public List<RadioDABStation> getChannels() {
        return channels;
    }
}
