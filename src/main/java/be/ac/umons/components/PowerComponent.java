package be.ac.umons.components;

import be.ac.umons.enumerations.PowerStatus;

public class PowerComponent extends RadioComponent{
    private PowerStatus powerStatus;

    public PowerComponent() {
        powerStatus = PowerStatus.OFF;
    }

    public PowerStatus changeStatus() {
        if (powerStatus == PowerStatus.OFF) {
            powerStatus = PowerStatus.ON;
        } else {
            powerStatus = PowerStatus.OFF;
        }
        return powerStatus;
    }

    public PowerStatus getPowerStatus() {
        return powerStatus;
    }
}
