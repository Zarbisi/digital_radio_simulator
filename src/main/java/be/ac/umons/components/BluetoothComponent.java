package be.ac.umons.components;

public class BluetoothComponent extends InputComponent{
    private boolean connected = false;

    public BluetoothComponent() {
        populateSongList();
        super.actualSong = super.songs.get(super.indexSong);
    }

    @Override
    protected void populateSongList() {
        super.songs.add("ACDC - Back in Black");
        super.songs.add("ACDC - ThunderStruck");
        super.songs.add("Skip the use - Cup Of Coffee");
        super.songs.add("Skip the use - Damn Cool");
        super.songs.add("Skip the use - Get Papers");
        super.songs.add("Skip the use - Ghost");
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}
