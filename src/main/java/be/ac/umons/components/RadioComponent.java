package be.ac.umons.components;

import be.ac.umons.enumerations.ComponentStatus;

public abstract class RadioComponent {
    private ComponentStatus componentStatus = ComponentStatus.NONACTIVATED;

    public ComponentStatus getStatus() {
        return componentStatus;
    }

    public boolean isFailing() {
        if (componentStatus == ComponentStatus.FAILURE) {
            return true;
        } else {
            return false;
        }
    }

    public void setFailure() {this.componentStatus = ComponentStatus.FAILURE;}

    public void setFunctional() {this.componentStatus = ComponentStatus.FUNCTIONAL;}

    public void setNonActivated() {this.componentStatus = ComponentStatus.NONACTIVATED;}
}
