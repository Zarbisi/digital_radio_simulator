package be.ac.umons.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BreakingNewsComponent extends RadioComponent {
    private List<String> news = new ArrayList<>();
    private int index;

    public BreakingNewsComponent() {
        populateNews();
    }

    private void populateNews() {
        news.add("COVID-19 News");
        news.add("USA News");
        news.add("War News");
        news.add("Student News");
    }

    private int randomIndex() {
        Random random = new Random();
        index = random.nextInt(news.size());
        return index;
    }

    public String getBroadcastNews() {
        return news.get(randomIndex());
    }

    protected void setNews (List<String> newsList) {
        this.news = newsList;
    }
}
