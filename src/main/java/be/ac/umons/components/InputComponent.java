package be.ac.umons.components;

import java.util.ArrayList;
import java.util.List;

public abstract class InputComponent extends RadioComponent {

    protected String actualSong;
    protected List<String> songs = new ArrayList<>();
    protected int indexSong = 0;

    protected abstract void populateSongList();

    public void next() {
        if (indexSong >= songs.size() - 1) {
            indexSong = 0;
        } else {
            indexSong++;
        }
    }

    public void previous() {
        if (indexSong <= 0) {
            indexSong = songs.size() - 1;
        } else {
            indexSong--;
        }
    }

    public String getActualSong() {
        actualSong = songs.get(indexSong);
        return actualSong;
    }

    public void setIndexSong(int indexSong) {
        this.indexSong = indexSong;
    }

    public int getIndexSong() {
        return indexSong;
    }

    public List<String> getSongs() {
        return songs;
    }
}
