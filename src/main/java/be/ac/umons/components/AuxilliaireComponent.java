package be.ac.umons.components;

public class AuxilliaireComponent extends InputComponent {

    public AuxilliaireComponent() {
        populateSongList();
        super.actualSong = super.songs.get(super.indexSong);
    }

    @Override
    protected void populateSongList() {
        super.songs.add("Shaka Ponk - Gung Ho");
        super.songs.add("Shaka Ponk - I'm Picky");
        super.songs.add("Sam Fender - The Borders");
        super.songs.add("Biffy Clyro - Instant History");
        super.songs.add("Ready the Prince - PB&J");
        super.songs.add("Andy Grammar - Fresh Eyes");
    }

}
