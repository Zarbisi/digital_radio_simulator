package be.ac.umons.components;

import be.ac.umons.models.RadioStation;

import java.util.ArrayList;
import java.util.List;

public class AutotuneFMComponent extends RadioComponent {
    Radio radio;
    private List<RadioStation> autotuneFMList = new ArrayList<>();

    public AutotuneFMComponent(Radio radio) {
        this.radio = radio;
    }

    public List<RadioStation> setAutotuneList() {
        autotuneFMList = new ArrayList<>();
        int baseIndex = radio.getTunerFM().getFrequencyIndex();
        int actualIndex =  radio.getTunerFM().getFrequencyIndex();
        for (int i=0; i < radio.getTunerFM().getStations().size(); i++) {
            autotuneFMList.add(radio.getTunerFM().getStations().get(actualIndex));
            if (actualIndex < radio.getTunerFM().getStations().size() - 1) {
                actualIndex++;
            } else if (actualIndex == baseIndex - 1) {
                break;
            } else {
                actualIndex = 0;
            }
        }
        return autotuneFMList;
    }
}
