module be.ac.umons {
    requires javafx.controls;
    requires javafx.fxml;

    opens be.ac.umons to javafx.fxml;
    opens be.ac.umons.controllers to javafx.fxml;
    exports be.ac.umons;
    exports be.ac.umons.controllers;
}