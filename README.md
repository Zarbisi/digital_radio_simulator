# Digital_Radio_Simulator

Ce projet est un travail pratique à réaliser dans le cadre du cours de Modélisation Logicielle dispensé dans le cadre de mon Master en Sciences Informatiques en horaire décalé.

## Technologies

- Java 12+
- JavaFX 13

## Contributions

Aucune contribution ne sera acceptée sur ce projet.

## ROADMAP

https://trello.com/b/dvSpLvdR/dab-radio-mod%C3%A9lisation-logicielle

## SOURCE Radio
http://radiomap.eu/be/charleroi